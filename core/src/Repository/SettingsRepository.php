<?php
namespace src\Repository;

use Respect\Validation\Rules\Date;
use src\Library\Repository;
use src\Library\Helper;

class SettingsRepository extends Repository {

    /**
     * Retrieve a setting from the database by the given key
     * @param $key
     * @return mixed
     */
    public function getSetting($key){
        $statement = $this->eduioDB->prepare("SELECT value FROM settings WHERE `key`=:id");
        $statement->execute([":id" => $key]);
        $value = $statement->fetchColumn();
        return $value;
    }

    /**
     * Retrieve the current year
     * @return mixed
     */
    public function getCurrentYearId(){
        $statement = $this->eduioDB->prepare("SELECT value FROM settings WHERE `key`='currentYear'");
        $statement->execute();
        $value = $statement->fetchColumn();
        if ($value === false){
            $statement = $this->eduioDB->prepare("INSERT INTO year (displayName) VALUES (:currentYear) ON DUPLICATE key UPDATE id=LAST_INSERT_ID(), displayName=:currentYear");
            $statement->execute([":currentYear" => date("Y")]);
            $value = $this->eduioDB->lastInsertId();
            $statement = $this->eduioDB->prepare("INSERT INTO settings (`key`,`value`) VALUES ('currentYear',$value) ON DUPLICATE KEY UPDATE value=$value");
            $statement->execute();
        }
        return $value;
    }

    /**
     * Set the current year
     * @param $currentYearId
     */
    public function setCurrentYear($currentYearId){
        $statement = $this->eduioDB->prepare("INSERT INTO settings (`key`,`value`) VALUES ('currentYear',:value) ON DUPLICATE KEY UPDATE value=:value");
        $statement->execute([":value" => $currentYearId]);
    }
}