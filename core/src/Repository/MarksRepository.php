<?php
namespace src\Repository;
use src\Library\Repository;

class MarksRepository extends Repository {

    /**
     * Retrieves all marks with specific userid from the db
     * @param $userId
     * @return array of marks or false if any error occured
     */
    public function getMarksOfUser($userId,$yearId){
        $statement = $this->eduioDB->prepare("SELECT mark.id, mark.mark, mark.isBigMark, subjects.name AS 'subjectName', subjects.id AS 'subjectId', DATE_FORMAT(mark.createDate, '%d.%m.%Y') AS 'date', mark.comment FROM mark, subjects WHERE userId = :userid AND mark.subjectId = subjects.id AND mark.yearId = :yearId ORDER BY subjects.name ASC, mark.createDate ASC");
        $statement->execute(array(":userid" => $userId, ":yearId" => $yearId));
        $marks = $statement->fetchAll();
        return $marks;
    }

    /**
     * Retrieves all marks of the specific combination of class and subject
     * @param $classId
     * @param $subjectId
     * @return array of marks or false if any error occured
     */
    public function getMarksOfClassSubject($classId, $subjectId, $yearId){
        $statement = $this->eduioDB->prepare("SELECT mark.id,
                                              user.id AS 'studentId',
                                              CONCAT(user.firstname,
                                              ' ',
                                              user.lastname) AS 'studentName',
                                              mark.mark,
                                              mark.isBigMark,
                                              DATE_FORMAT(mark.createDate, '%d.%m.%Y') AS 'date',
                                              mark.comment
                                              FROM mark JOIN (form,studentForm, user) 
                                              ON (mark.userId = user.id 
                                                  AND form.id = studentForm.formId 
                                                  AND user.id= studentForm.userId) 
                                              WHERE form.id =:classId AND mark.subjectId=:subjectId
                                              AND mark.yearId = :yearId ORDER BY studentName ASC, mark.createDate ASC");
        $statement->execute(array(":classId" => $classId, ":yearId" => $yearId, ":subjectId" => $subjectId));
        $marks = $statement->fetchAll();
        return $marks;
    }

    public function addMarkOfClassSubject($mark, $userId, $subjectId, $isBigMark, $comment, $yearId){
        $query= "INSERT INTO mark (mark, yearId, createDate, userId, subjectId, isBigMark, comment) VALUES ".
            "(:mark, :yearId, :createDate, :userId, :subjectId, :isBigMark, :comment)";
        $statement = $this->eduioDB->prepare($query);
        $result = $statement->execute(array(
            ":mark"=>$mark,
            ":yearId"=> $yearId,
            ":createDate"=>date('Y-m-d',time()),
            ":userId"=>$userId,
            ":subjectId"=>$subjectId,
            ":isBigMark"=>$isBigMark,
            ":comment"=>$comment,
        ));
        if (!$result){
            return false;
        }
    }

    public function deleteMark($markId){
        $query= "DELETE FROM mark WHERE id=:id";
        $parameters[':id'] = $markId;
        $statement = $this->eduioDB->prepare($query);
        return $statement->execute($parameters);
    }


    /**
     * Checks if the given mark really exists and belongs to the given student and subject
     * @param $markId
     * @param $studentId
     * @param $subjectId
     * @return bool true if the mark exists and belongs to the student and the subject, false if it doesn't exist or
     * belongs to another student or subject
     */
    public function checkMark($markId,$studentId,$subjectId){
        $statement = $this->eduioDB->prepare("SELECT userId, subjectId FROM mark WHERE id=:markid");
        $statement->execute(array(":markid" => $markId));
        $mark = $statement->fetch();
        if ($mark === false){
            return false;
        }else{
            if ($mark['userId'] == $studentId && $mark['subjectId'] == $subjectId){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * Updates the mark with the given data
     * data has to be an array that can have following indexes:
     *  - mark
     *  - createDate
     *  - comment
     * Updating yearId,userId,subjectId or isBigMark is not intended to be done by this method. Create
     * a new Mark and delete the old mark in case you want to change one of these values.
     * @param $markId
     * @param $data
     * @return bool
     */
    public function updateMark($markId, $data){
        $fields = ['mark', 'createDate', 'comment'];
        $query = "UPDATE mark SET ";
        $parameters = [];
        $executeQuery = false;
        foreach ($data as $key => $value){
            if (in_array($key,$fields)){
                $executeQuery = true;
                $query .= "$key = :$key, ";
                $parameters[":$key"] = $value;
            }
        }
        $query = substr($query,0,strlen($query)-2);
        $query .= " WHERE id = :id";
        $parameters[":id"] = $markId;
        if ($executeQuery) {
            $statement = $this->eduioDB->prepare($query);
            foreach ($parameters as $key => $value){
                $statement->bindValue($key,$value);
            }
            return $statement->execute();
        }else{
            return true;
        }
    }

    /**
     * insert the specified mark into the database. Does not check if the given data is valid! Use Helper::validate() to do this
     * @param $data - indexed array of mark data
     *              must have values: mark, createDate, comment, yearId, userId, subjectId, isBigMark
     * @return mixed id of inserted mark on success, or false on failure
     */
    public function createMark($data){
        $query= "INSERT INTO mark (mark, createDate, comment, yearId , userId, subjectId, isBigMark) VALUES ".
            "(:mark, :createDate, :comment, :yearId, :userId, :subjectId, :isBigMark)";
        $statement = $this->eduioDB->prepare($query);
        $result = $statement->execute($data);
        if (!$result){
            return false;
        }
        return $this->eduioDB->lastInsertId();
    }

}