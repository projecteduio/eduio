<?php
namespace src\Repository;

use Respect\Validation\Rules\Date;
use src\Library\Repository;
use src\Library\Helper;

class TokenRepository extends Repository {

    /**
     * Generates a token for the specified user and writes it into the database.
     * In case of a user already possessing more than 4 tokens in the database, all existing tokens will be
     * deleted and only after that the new token is generated
     * @param $userid
     * @return string the generated token
     */
    public function generateToken($userid){
        $statement = $this->eduioDB->prepare("SELECT * FROM token WHERE userid=:userid;");
        $statement->execute(array (":userid" => $userid));
        $i = 0; // counter of tokens for this user in the database
        $time = new \DateTime();
        $time->add(\DateInterval::createFromDateString("-10 minutes"));
        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)){
            if ($row['time'] < $time->getTimestamp()){ // delete tokens that are expired ( older than 10 minutes)
                $deleteStatement = $this->eduioDB->prepare("DELETE FROM token WHERE id=:id;");
                $deleteStatement->execute(array(":id" => $row["id"]));
            }else{
                $i++; // count number of valid tokens in database
            }
        }
        if ($i >= 5){ // user exceeds maximum limit of tokens -> delete all existing tokens and afterwards generate a new token
            $deleteStatement = $this->eduioDB->prepare("DELETE FROM token WHERE userid=:userid;");
            $deleteStatement->execute(array (":userid" => $userid));

            $token = Helper::generateRandomString(50);
            $insertStatement = $this->eduioDB->prepare("INSERT INTO token (hash, time, userid) VALUES (:token, :time, :userid)");
            $insertStatement->execute(array(":token" => $token, ":time" => (new \DateTime())->getTimestamp(), ":userid" => $userid));
        }else{ // generate new token
            $statement = $this->eduioDB->prepare("SELECT * FROM token WHERE hash=:token AND userid=:userid;");
            do{
                $token = Helper::generateRandomString(50);
                $statement->execute(array(":token" => $token, ":userid" => $userid));
            }while ($statement->fetch() !== false);
            $insertStatement = $this->eduioDB->prepare("INSERT INTO token (hash, time, userid) VALUES (:token, :time, :userid)");
            $insertStatement->execute(array(":token" => $token, ":time" => (new \DateTime())->getTimestamp(), ":userid" => $userid));
        }
        return $token;
    }

    /**
     * Checks if the given token is valid for the user with the given userid
     * Please note that calling this function deletes and therefore invalidates all tokens
     * older than the given token of the user
     * @param $token
     * @param $userid
     * @return bool true - token is valid, false -  token is invalid
     */
    public function checkToken($token, $userid){
        $statement = $this->eduioDB->prepare("SELECT * FROM token WHERE hash=:token AND userid=:userid ;");
        $statement->execute(array(":token" => $token, ":userid" => $userid));
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if ($result === false){ // token not existing in database -> token is invalid
            return false;
        }else { // token exists and is therefore valid

            $time = new \DateTime();
            $time->add(\DateInterval::createFromDateString("-10 minutes"));
            if ($result['time'] < $time->getTimestamp()){ // token exists but is too old
                return false;
            }else{ // token exists and is valid

                // delete all tokens that are older than the given token because they aren't needed anymore and they
                // clutter the database otherwise
                $allTokensStatement = $this->eduioDB->prepare("SELECT * FROM token WHERE userid=:userid ;");
                $allTokensStatement->execute(array(":userid" => $userid));
                while ($row = $allTokensStatement->fetch(\PDO::FETCH_ASSOC)){
                    if ($row['time'] < $result['time']){
                        $deleteStatement =  $this->eduioDB->prepare("DELETE FROM token WHERE id=:id;");
                        $deleteStatement->execute(array(":id" => $row["id"]));
                    }
                }

                return true;
            }
        }
    }
}