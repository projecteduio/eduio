<?php
namespace src\Repository;

use Respect\Validation\Rules\Date;
use src\Library\Repository;
use src\Library\Helper;

class YearsRepository extends Repository {

    /**
     * Retrieve the current year
     * @return mixed
     */
    public function getCurrentYear(){
        $statement = $this->eduioDB->prepare("SELECT value FROM settings WHERE `key`='currentYear'");
        $statement->execute();
        $id = $statement->fetchColumn();
        if ($id === false){
            $years = $this->getYears();
            if ($years != null){
                $id = $years[count($years)-1]['id'];
                $displayName = $years[count($years)-1]['displayName'];
                $statement = $this->eduioDB->prepare("INSERT INTO settings (`key`,`value`) VALUES ('currentYear',:yearId) ON DUPLICATE KEY UPDATE value=:yearId");
                $statement->execute([":yearId" => $id]);
            }else{
                $displayName = date("Y");
                $statement = $this->eduioDB->prepare("INSERT INTO year (displayName) VALUES (:displayName)");
                $statement->execute([":displayName" => $displayName]);
                $id = $this->eduioDB->lastInsertId();
                $statement = $this->eduioDB->prepare("INSERT INTO settings (`key`,`value`) VALUES ('currentYear',:yearId) ON DUPLICATE KEY UPDATE value=:yearId");
                $statement->execute([":yearId" => $id]);
            }
            return ['id' => $id, 'displayName' => $displayName];
        }
        $statement = $this->eduioDB->prepare("SELECT * FROM year WHERE id=:id");
        $statement->execute([":id" => $id]);
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if ($result === false){
            $years = $this->getYears();
            if ($years != null){
                $id = $years[count($years)-1]['id'];
                $displayName = $years[count($years)-1]['displayName'];
                $statement = $this->eduioDB->prepare("INSERT INTO settings (`key`,`value`) VALUES ('currentYear',:yearId) ON DUPLICATE KEY UPDATE value=:yearId");
                $statement->execute([":yearId" => $id]);
            }else{
                $displayName = date("Y");
                $statement = $this->eduioDB->prepare("INSERT INTO year (displayName) VALUES (:displayName)");
                $statement->execute([":displayName" => $displayName]);
                $id = $this->eduioDB->lastInsertId();
                $statement = $this->eduioDB->prepare("INSERT INTO settings (`key`,`value`) VALUES ('currentYear',:yearId) ON DUPLICATE KEY UPDATE value=:yearId");
                $statement->execute([":yearId" => $id]);
            }
            return ['id' => $id, 'displayName' => $displayName];
        }else{
            return $result;
        }
    }

    /**
     * Retrieve all years
     * @return mixed
     */
    public function getYears(){
        $statement = $this->eduioDB->prepare("SELECT * FROM year");
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * Retrieve a year by its id
     * @return bool
     */
    public function getYearById($id){
        $statement = $this->eduioDB->prepare("SELECT * FROM year WHERE id=:id");
        $statement->execute([":id" => $id]);
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }


    /**
     * Set the current year
     * @param $currentYearId
     * @return boolean false if the year does no exist, true on success
     */
    public function setCurrentYear($currentYearId){
        $statement = $this->eduioDB->prepare("SELECT * FROM year WHERE id=:id");
        $statement->execute([":id" => $currentYearId]);
        if ($statement->fetch(\PDO::FETCH_ASSOC) === false){
            return false;
        }
        $statement = $this->eduioDB->prepare("INSERT INTO settings (`key`,`value`) VALUES ('currentYear',:value) ON DUPLICATE KEY UPDATE value=:value");
        $statement->execute([":value" => $currentYearId]);
        return true;
    }

    /**
     * Updates the specified year, or inserts it if it doesn't exist
     * @param $yearId
     * @param $displayName - the new displayName
     * @return boolean - true on success, false on failure
     */
    public function updateYear($yearId, $displayName){
        $statement = $this->eduioDB->prepare("INSERT INTO year (`id`,`displayName`) VALUES (:id,:displayName) ON DUPLICATE KEY UPDATE displayName=:displayName");
        return $statement->execute([":id" => $yearId, ":displayName" => $displayName]);
    }

    /**
     * Deletes the specifid year. Be careful! This also deletes all marks that are associated with this year!
     * @param $yearId
     * @return boolean - true on success, false on failure
     */
    public function deleteYear($yearId){
        $statement = $this->eduioDB->prepare("DELETE FROM year WHERE id=:id");
        return $statement->execute([":id" => $yearId]);
    }

    /**
     * Creates a new year with the given display name
     * @param $displayName
     * @return boolean - true on success, false on failure
     */
    public function createYear($displayName){
        $statement = $this->eduioDB->prepare("INSERT INTO year (`displayName`) VALUES (:displayName)");
        $result = $statement->execute([":displayName" => $displayName]);
        if (!$result){
            return false;
        }
        return $this->eduioDB->lastInsertId();
    }
}