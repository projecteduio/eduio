<?php
namespace src\Repository;

use Respect\Validation\Rules\Date;
use src\Library\Repository;

class ClassesRepository extends Repository {
    /**
     * Retrieves all classes of the teacher with the specified userid
     * @return array of classes or false if any error occured
     */
    public function getClassesOfTeacher($userId){
        $statement = $this->eduioDB->prepare("SELECT formID as 'id', displayName FROM permission, form WHERE permission.formID = form.id AND permission.userId = :userid GROUP BY formID");
        $statement->execute(array(":userid" => $userId));
        $classes = $statement->fetchAll();
        return $classes;
    }

    /**
     * Retrieves all classes of the teacher with the specified userid
     * @return array of classes or false if any error occured
     */
    public function getClassOfClassTeacher($userId){
        $statement = $this->eduioDB->prepare("SELECT * FROM form WHERE classTeacher=:userid");
        $statement->execute(array(":userid" => $userId));
        return $statement->fetch();
    }

    /**
     * Retrieves all classes along with their class teacher id, firstname and lastname as an array of rows
     * example row :
     * {
     *      'id' => 1,
     *      'displayName' => '1B',
     *      'classTeacher' => 32,
     *      'firstname'  => 'Max',
     *      'lastname'  => 'Mueller'
     * }
     * @return mixed
     */
    public function getClasses(){
        $statement = $this->eduioDB->prepare("SELECT * FROM form");
        $statement->execute();
        $classes = $statement->fetchAll();
        return $classes;
    }

    /**
     * Retrieves the students that are part of this class
     * @param $classid
     * @return mixed array of users (the students)
     */
    public function getStudentsOfClass($classid){
        $statement = $this->eduioDB->prepare("SELECT user.*, studentForm.formid AS 'classid',form.displayName AS 'class' FROM user LEFT JOIN (form,studentForm) ON(user.id=studentForm.userId AND form.id=studentForm.formId) WHERE formId = :formid ORDER BY user.lastname;");
        $statement->execute(array("formid" => $classid));
        $students = $statement->fetchAll();
        return $students;
    }

    /**
     * Retrieves the number of students of a specific class
     * @param $classid
     * @return integer the number of students
     */
    public function getStudentsCountOfClass($classid){
        $statement = $this->eduioDB->prepare("SELECT COUNT(*) AS 'count' FROM studentForm WHERE formId = :formid;");
        $statement->execute(array("formid" => $classid));
        $count = $statement->fetch()['count'];
        return $count;
    }

    /**
     * Retrieves the user data of the teacher of the specific class
     * @param $classid
     * @return mixed - the user data of the teacher or false if the class does not exist or the class has no class teacher
     */
    public function getClassTeacherOfClass($classid){
        $statement = $this->eduioDB->prepare("SELECT user.* FROM user,form WHERE user.id=form.classTeacher AND form.id=:classid;");
        $statement->execute(array(':classid' => $classid));
        $teacher= $statement->fetch();
        return $teacher;
    }

    /**
     * Retrieves the permission that a user has for a specific class and subject
     * @param $classid
     * @return mixed 0(read) or 1(write) if a permission exists. If no permission exists, false is returned.
     */
    public function getPermission($userId,$classId,$subjectId){
        $statement = $this->eduioDB->prepare("SELECT access FROM permission WHERE userId=:userId AND formId=:classId AND subjectId=:subjectId");
        $statement->execute(array(':classId' => $classId,':userId' => $userId, ':subjectId' => $subjectId));
        $result = $statement->fetch();
        if ($result === false){
            return false;
        }else{
            return $result['access'];
        }
    }

    /**
     * Retrieves the user data of the class teacher of the specified student
     * @param $studentId
     * @return mixed - the user data of the teacher or false if the student does not exist or the student has no class teacher
     */
    public function getClassTeacherOfUser($studentId){
        $statement = $this->eduioDB->prepare("SELECT form.classTeacher FROM user JOIN (studentForm,form) ON (studentForm.userId=user.id AND form.id=studentForm.formId) WHERE user.id = :studentId");
        $statement->execute(array(":studentId" => $studentId));
        $teacher = $statement->fetch(\PDO::FETCH_ASSOC);
        if ($teacher === false){
            return false;
        }else{
            $statement = $this->eduioDB->prepare("SELECT * FROM user WHERE id=:teacherId;");
            $statement->execute(array(":teacherId" => $teacher['classTeacher']));
            $teacher = $statement->fetch(\PDO::FETCH_ASSOC);
            return $teacher;
        }
    }

    /**
     * Retrieves all subjects of a class
     * @param $formID
     * @return mixed array of subjects
     */
    public function getSubjectsOfClass($formID){
        $statement = $this->eduioDB->prepare("SELECT subjects.id, subjects.name FROM permission, subjects WHERE permission.subjectID = subjects.id AND permission.formId = :subjectid GROUP BY subjectID");
        $statement->execute(array(":subjectid" => $formID));
        $subjects = $statement->fetchAll();
        return $subjects;
    }

    /**
     * Retrieves the class of a user if he is a student or false if he is not part of any class
     * @param $userid
     * @return mixed the class
     */
    public function getClassOfUser($userid){
        $statement = $this->eduioDB->prepare("SELECT form.* FROM form,studentForm,user WHERE form.id=studentForm.formId AND user.id=studentForm.userId AND user.id=:userid");
        $statement->execute(array(":userid" => $userid));
        $class = $statement->fetch();
        return $class;
    }

    /**
     * Retrieves a class by its id
     * @param $classId
     * @return mixed the class
     */
    public function getClassById($classId){
        $statement = $this->eduioDB->prepare("SELECT * FROM form WHERE id=:classId");
        $statement->execute(array(':classId' => $classId));
        $classes = $statement->fetch(\PDO::FETCH_ASSOC);
        return $classes;
    }

    /**
     * Creates a new class. Does not check if the given data is valid! Use Helper::validate() to do this
     * @param $data - indexed array of class data
     *              possible keys: displayName, classTeacher
     *              if $data does not contain any of the keys, the function still returns true but doesn't do anything
     * @return true on success, false on failure
     */
    public function createClass($data){
        $fields = ['displayName', 'classTeacher'];
        $query = "INSERT INTO form ";
        $parameters = [];
        $executeQuery = false;
        $columns = "(";
        $values = " VALUES (";
        foreach ($data as $key => $value){
            if (in_array($key,$fields)){
                $executeQuery = true;
                $columns .= "$key, ";
                $values  .= ":$key, ";
                $parameters[":$key"] = $value;
            }
        }
        $columns = substr($columns,0,strlen($columns)-2).")";
        $values = substr($values,0,strlen($values)-2).")";
        $query .= $columns.$values;
        if ($executeQuery) {
            $statement = $this->eduioDB->prepare($query);
            $result = $statement->execute($parameters);
            if (!$result){
                return false;
            }
            return $this->eduioDB->lastInsertId();
        }else{
            return false;
        }
    }

    /**
     * Updates the specified class. Does not check if the given data is valid! Use Helper::validate() to do this
     * @param $data - indexed array of class data
     *              possible keys: displayName, classTeacher
     *              if $data does not contain any of the keys, the function still returns true but doesn't do anything
     * @return true on success, false on failure
     */
    public function updateClass($classId, $data){
        $fields = ['displayName', 'classTeacher'];
        $query = "UPDATE form SET ";
        $parameters = [];
        $executeQuery = false;
        foreach ($data as $key => $value){
            if (in_array($key,$fields)){
                $executeQuery = true;
                $query .= "$key = :$key, ";
                $parameters[":$key"] = $value;
            }
        }
        $query = substr($query,0,strlen($query)-2);
        $query .= " WHERE id = :id";
        $parameters[":id"] = $classId;
        if ($executeQuery) {
            $statement = $this->eduioDB->prepare($query);
            foreach ($parameters as $key => $value){
                $statement->bindValue($key,$value);
            }
            return $statement->execute();
        }else{
            return true;
        }
    }

    /**
     * Deletes the specified class
     * @param $classId
     * @return mixed true on success, false on failure
     */
    public function deleteClass($classId){
        $query = "DELETE FROM form WHERE id=:id";
        $parameters[':id'] = $classId;
        $statement = $this->eduioDB->prepare($query);
        return $statement->execute($parameters);
    }
}