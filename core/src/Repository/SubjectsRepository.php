<?php
/**
 * Created by PhpStorm.
 * User: lbram
 * Date: 13.12.2016
 * Time: 12:31
 */

namespace src\Repository;


use src\Library\Repository;

class SubjectsRepository extends Repository
{
    public function getSubjects(){
        $statement = $this->eduioDB->prepare("SELECT * FROM subjects");
        $statement->execute();
        $subjects = $statement->fetchAll();
        return $subjects;
    }

    public function getSubjectById($subjectId){
        $statement = $this->eduioDB->prepare("SELECT * FROM subjects WHERE id=:subjectId");
        $statement->execute(array(':subjectId' => $subjectId));
        $subjects = $statement->fetch(\PDO::FETCH_ASSOC);
        return $subjects;
    }
}