<?php
namespace src\Repository;

use Respect\Validation\Rules\Date;
use src\Library\Repository;

class UserRepository extends Repository {

    /**
     * Retrieves all users from the db
     * @param $withClass - true, if class of user should be retrieved too. default false
     * @return array of users or false if any error occured
     */
    public function getUsers($withClass = false){
        if ($withClass) {
            $statement = $this->eduioDB->prepare("SELECT user.*, studentForm.formid AS 'classid',form.displayName AS 'class' FROM user LEFT JOIN (form,studentForm) ON(user.id=studentForm.userId AND form.id=studentForm.formId) ORDER BY user.lastname;");
        }else{
            $statement = $this->eduioDB->prepare("SELECT * FROM user ORDER BY lastname ASC");
        }
        $statement->execute();
        $users = $statement->fetchAll();
        return $users;
    }

    /**
     * Checks if token exists and is valid
     * @param $token
     * @return bool
     */
    public function checkToken($token, $userId){
        $statement = $this->eduioDB->prepare("SELECT * FROM user WHERE hash=:token AND id=:userid ;");
        $statement->execute(array(":token" => $token, ":userid" => $userId));
        $result = $statement->fetch();
        if ($result == false){
            return false;
        }else {
            $time = new \DateTime();
            $time->add(\DateInterval::createFromDateString("-10 minutes"));
            if ($result['hash_timestamp'] < $time->getTimestamp()){
                return false;
            }else{
                return true;
            }
        }
    }

    /**
     * Fetches the user with the specified userid from the db
     * @param $userId
     * @throws PDOException if any db error occured
     * @return $user - indexed array with column names as keys or false if user does not exist or an error occured
     */
    public function getUserById($userId){
        $statement = $this->eduioDB->prepare("SELECT user.*,form.id AS 'classId', form.displayName AS 'class' FROM user LEFT JOIN (studentForm, form) ON (studentForm.userId = user.id AND studentForm.formId = form.id) WHERE user.id= :userid ");
        $statement->execute(array(":userid" => $userId));
        $user = $statement->fetch(\PDO::FETCH_ASSOC);
        return $user;
    }

    /**
     * Fetches the user with the specified username from the db
     * @param $username
     * @return $user - indexed array with column names as keys or false if username does not exist or an error occured
     */
    public function getUserByUsername($username){
        $statement = $this->eduioDB->prepare("SELECT * FROM user WHERE username= :username");
        $statement->execute(array(":username" => $username));
        $user = $statement->fetch(\PDO::FETCH_ASSOC);
        return $user;
    }

    /**
     * Updates the token of the user with the specified id
     * @param $userId
     * @param $token
     */
    public function updateTokenForUser($userId,$token)
    {
        $time = (new \DateTime())->getTimestamp();
        $statement = $this->eduioDB->prepare("UPDATE user SET hash = :token, hash_timestamp = :time WHERE id= :userid");
        $statement->execute(array(":token" => $token, ":userid" => $userId, ":time" => $time));
    }

    /**
     * Updates the specified user. Does not check if the given data is valid! Use Helper::validate() to do this
     * @param $data - indexed array of user data
     *              possible values: id (always needed!), firstname, lastname, type, active, password, gender
     * @return true on success, false on failure
     */
    public function updateUser($data){
        if (!isset($data['id']))
            return false;
        $fields = ['firstname', 'lastname', 'type', 'active', 'password', 'gender'];
        $query = "UPDATE user SET ";
        $parameters = [];
        $executeQuery = false;
        foreach ($data as $key => $value){
            if (in_array($key,$fields)){
                $executeQuery = true;
                $query .= "$key = :$key, ";
                $parameters[":$key"] = $value;
            }
        }
        $query = substr($query,0,strlen($query)-2);
        $query .= " WHERE id = :id";
        $parameters[":id"] = $data['id'];
        if ($executeQuery) {
            $statement = $this->eduioDB->prepare($query);
            return $statement->execute($parameters);
        }else{
            return true;
        }
    }

    /**
     * Creates the specified user. Does not check if the given data is valid! Use Helper::validate() to do this
     * @param $data - indexed array of user data
     *              must have values: id , firstname, lastname, username, type, active, password, gender
     * @return mixed id of created user on success, or false on failure
     */
    public function createUser($data){
        $query= "INSERT INTO user (username, firstname, lastname, type , active, password, gender) VALUES ".
                "(:username, :firstname, :lastname, :type, :active, :password, :gender)";
        $statement = $this->eduioDB->prepare($query);
        $result = $statement->execute($data);
        if (!$result){
            return false;
        }
        return $this->eduioDB->lastInsertId();
    }

    /**
     * Updates the class of the user
     * @param $userId
     * @param $classId
     * @return true on success, false on failure
     */
    public function updateClassOfUser($userId, $classId){
        $statement = $this->eduioDB->prepare("INSERT INTO studentForm (userId,formId) VALUES (:userId, :classId) ON DUPLICATE KEY UPDATE formId=:classId;");
        return $statement->execute(array(":userId" => $userId, "classId" => $classId));
    }

    /**
     * Removes the user from his current class
     * @param $userId
     * @return true on success, false on failure
     */
    public function removeClassOfUser($userId){
        $statement = $this->eduioDB->prepare("DELETE FROM studentForm WHERE userId=:userId;");
        return $statement->execute(array(":userId" => $userId));
    }

    /**
     * deletes the user with the specified id
     * @param $userid
     * @return boolean - false on failure
     */
    public function deleteUser($userid){
        $query = "DELETE FROM user WHERE id=:id";
        $parameters[':id'] = $userid;
        $statement = $this->eduioDB->prepare($query);
        return $statement->execute($parameters);
    }

    /**
     * deletes all users whose ids are in the $userIds array
     * @param $userIds - array containing the ids of the users who should be deleted
     * @return boolean - false on failure
     */
    public function deleteUsers($userIds){
        $placeholders = array_fill(0,count($userIds), "?");
        $placeholders = implode(",",$placeholders);
        $query = "DELETE FROM user WHERE id in ($placeholders)";
        $statement = $this->eduioDB->prepare($query);
        return $statement->execute($userIds);
    }

    /**
     * Generates a unique username from the specified firstname and lastname
     * @param $firstname
     * @param $lastname
     * @return string
     */
    public function generateUsername($firstname, $lastname){
        $username = strtolower($lastname.".".$firstname);
        $i = "";
        while ($this->getUserByUsername($username.$i) !== false){
            if ($i === ""){
                $i = 1;
            }else{
                $i++;
            }
        }

        return $username.$i;
    }

    /**
     * Fetches the permissions of the user with the specified id
     * @param $userid
     * @return $user - indexed array with column names as keys or false if username does not exist or an error occured
     */
    public function getPermissionsOfUser($userId){
        $statement = $this->eduioDB->prepare("SELECT id, subjectId, formId as classId, access FROM permission WHERE userId=:userId");
        $statement->execute(array(":userId" => $userId));
        $permissions = $statement->fetchAll();
        return $permissions;
    }

    /**
     * Overwrites the permissions of the specified user with the given permissions
     * @param $userId
     * @param $permissions
     * @return true on success, false on failure
     */
    public function updatePermissionsOfUser($userId, $permissions){
        $deleted = $this->deletePermissionsOfUser($userId);
        if ($deleted === false){
            return false;
        }

        $valCount = count($permissions);
        $valCount *= 4;

        $query = "INSERT INTO permission (userId, subjectId, formId, access) VALUES ";

        $query .= implode(',', array_map(
            function($el) { return '('.implode(',', $el).')'; },
            array_chunk(array_fill(0, $valCount, '?'), 4)
        ));

        $values = array();
        foreach ($permissions as $permission){
            array_push($values,$userId);
            foreach ($permission as $value) {
                array_push($values, $value);
            }
        }
        $statement = $this->eduioDB->prepare($query);
        return $statement->execute($values);
    }

    /**
     * Deletes all permissions of the specified user
     * @param $userId
     */
    public function deletePermissionsOfUser($userId){
        $statement = $this->eduioDB->prepare("DELETE FROM permission WHERE userId=:userId");
        return $statement->execute(array(":userId" => $userId));
    }

}