<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        //DB SETTINGS
        'db' => [
//          'host' => 'localhost',
//          'user' => 'eduio',
//          'pw' => 'asdfgh88',
            'host' => 'mysql.eduio.net',
            'user' => 'eduio',
            'pw' => 'asdfgh88',
            'dbname' => 'eduio_dev',
            'salt' => 'zkJDSqXPFukGF1yOODonaftmUeyneATDtwMJDWZlIk87LyBhoEh8Db9qs4ybMLumePDw73hJ'
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
];
