<?php
namespace src\Controller;
use src\Library\Helper;

class Marks extends Controller {

    public function getMarksOfUserAction(){
        $params = $this->request->getParsedBody();
        $userId = $this->args['userId'];
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null; //currently logged in user
            if ($cuser === false
                || $cuser == null
                || ($cuser['type'] == 2 && $cuser['id'] != $userId) // the student himself is allowed to see his marks
                || ($cuser['type'] == 1 && $this->repository('Classes')->getClassTeacherOfUser($userId)['id'] !== $cuser['id']) // the class teacher of the student is allowed to see his marks
                || $cuser['type'] == 0){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $user = $this->repository('User')->getUserById($userId);
                if ($user === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_USER_ID']
                    ];
                } else {
                    $currentYearId = $this->repository("Settings")->getCurrentYearId();
                    $marks = $this->repository('Marks')->getMarksOfUser($userId,$currentYearId);
                    $subjects = [];
                    foreach ($marks as $mark) {
                        if (!isset($subjects[$mark['subjectId']])){
                            $subjects[$mark['subjectId']] = [
                                'subjectId' => $mark['subjectId'],
                                'subject' => $mark['subjectName'],
                                'small' => [],
                                'big' => []
                            ];
                        }
                        if($mark['isBigMark']==0){
                            array_push($subjects[$mark['subjectId']]['small'], array( 'id' => $mark['id'], 'mark' => $mark['mark'], 'comment' => $mark['comment'], 'date' => Helper::formatDateToGerman($mark['date'])));
                        }else{
                            array_push($subjects[$mark['subjectId']]['big'], array( 'id' => $mark['id'], 'mark' => $mark['mark'], 'comment' => $mark['comment'], 'date' => Helper::formatDateToGerman($mark['date'])));
                        }
                    }
                    $marks = [];
                    foreach($subjects AS $subject){
                        array_push($marks,$subject);
                    }
                    $response = [
                        'marks' => $marks
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function getMarksOfSubjectAction(){
        $params = $this->request->getParsedBody();
        $subjectId = $this->args['subjectId'];
        $classId = $this->args['classId'];
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null; //currently logged in user
            if ($cuser === false
                || $cuser == null
                || ($cuser['type'] != 1 && $cuser['type'] != 3) // only teachers are allowed to see the marks of a subject
                || ($this->repository('Classes')->getClassTeacherOfClass($classId)['id'] !== $cuser['id'] && $cuser['type'] != 3
                    && $this->repository('Classes')->getPermission($cuser['id'],$classId,$subjectId) === false)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $class = $this->repository('Classes')->getClassById($classId);
                $subject = $this->repository('Subjects')->getSubjectById($subjectId);
                $permission = $this->repository('Classes')->getPermission($cuser['id'],$classId,$subjectId);
                if ($class === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_CLASS_ID']
                    ];
                } else if ($subject === null) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_SUBJECT_ID']
                    ];
                }else{
                    $currentYearId = $this->repository("Settings")->getCurrentYearId();
                    $marks = $this->repository('Marks')->getMarksOfClassSubject($classId,$subjectId,$currentYearId);
                    $students = [];
                    foreach ($marks as $mark) {
                        if (!isset($students[$mark['studentId']])){
                            $students[$mark['studentId']] = [
                                'studentId' => $mark['studentId'],
                                'student' => $mark['studentName'],
                                'small' => [],
                                'big' => []
                            ];
                        }
                        if($mark['isBigMark']==0){
                            array_push($students[$mark['studentId']]['small'], array( 'id' => $mark['id'],'mark' => $mark['mark'], 'comment' => $mark['comment'], 'date' => Helper::formatDateToGerman($mark['date'])));
                        }else{
                            array_push($students[$mark['studentId']]['big'], array( 'id' => $mark['id'],'mark' => $mark['mark'], 'comment' => $mark['comment'], 'date' => Helper::formatDateToGerman($mark['date'])));
                        }
                    }
                    $response = [
                        'marks' => $students
                    ];
                    if ($permission !== false){
                        $response['permission'] = $permission;
                    }else{
                        $response['permission'] = 0;
                    }
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function addMarksOfSubjectAction(){
        $params = $this->request->getParsedBody();
        $subjectId = $this->args['subjectId'];
        $classId = $this->args['classId'];
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null; //currently logged in user
            if ($cuser === false
                || $cuser == null
                || ($cuser['type'] != 1 && $cuser['type'] != 3) // only teachers are allowed to see the marks of a subject
                || ($this->repository('Classes')->getClassTeacherOfClass($classId)['id'] !== $cuser['id'] && $cuser['type'] != 3
                    && $this->repository('Classes')->getPermission($cuser['id'],$classId,$subjectId) != 1)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            } else {
                $class = $this->repository('Classes')->getClassById($classId);
                $subject = $this->repository('Subjects')->getSubjectById($subjectId);
                if ($class === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_CLASS_ID']
                    ];
                } else if ($subject === null) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_SUBJECT_ID']
                    ];
                }else{
                    $marks = json_decode($params['marks']);
                    foreach ($marks as $mark){
                        $this->repository('Marks')->addMarkOfClassSubject($mark->mark, $mark->userid, $subjectId, $mark->isBigMark, $mark->comment);
                    }
                    $response = [
                        'marks' => 1
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function deleteMarksOfSubjectAction(){
        $params = $this->request->getParsedBody();
        $subjectId = $this->args['subjectId'];
        $classId = $this->args['classId'];
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null; //currently logged in user
            if ($cuser === false
                || $cuser == null
                || ($cuser['type'] != 1 && $cuser['type'] != 3) // only teachers are allowed to see the marks of a subject
                || ($this->repository('Classes')->getClassTeacherOfClass($classId)['id'] !== $cuser['id'] && $cuser['type'] != 3
                    && $this->repository('Classes')->getPermission($cuser['id'],$classId,$subjectId) != 1)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            } else {
                $class = $this->repository('Classes')->getClassById($classId);
                $subject = $this->repository('Subjects')->getSubjectById($subjectId);
                if ($class === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_CLASS_ID']
                    ];
                } else if ($subject === null) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_SUBJECT_ID']
                    ];
                }else{
                    $marks = json_decode($params['marks']);
                    foreach ($marks as $mark){
                        $this->repository('Marks')->updateMarkOfClassSubject($mark->id);
                    }
                    $response = [
                        'marks' => 1
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function updateMarksOfSubjectAction(){
        $classId = $this->args['classId'];
        $subjectId = $this->args['subjectId'];
        $params = $this->request->getParsedBody();
        try{
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null; //currently logged in user
            if (!isset($params['userid']) || ($this->repository('Classes')->getPermission($params['userid'],$classId,$subjectId) !== "1" && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else{
                $response = "";
                $marks = isset($params['marks']) ? $params['marks'] : "";
                $updates = [];
                $deletes = [];
                $adds = [];
                $progress = 0;
                if (is_array($marks)){
                    foreach ($marks as $markId => $action){
                        if (!isset($action['type'])
                            || !isset($action['mark'])
                            || !isset($action['date'])
                            || !isset($action['comment'])
                            || !isset($action['subjectId'])
                            || !isset($action['studentId'])){
                            break;
                        }
                        $studentClass = $this->repository('Classes')->getClassOfUser($action['studentId']);
                        if ($studentClass === false || $studentClass['id'] != $classId)
                            break;
                        if ($action['subjectId'] != $subjectId)
                            break;
                        switch ($action['type']){
                            case "UPDATE":
                                if (!$this->repository('Marks')->checkMark($markId,$action['studentId'],$action['subjectId']))
                                    break 2;
                                if (!Helper::validate($action['mark'],'integer',["minvalue" => 1, "maxvalue" => 6]))
                                    break 2;
                                if (!Helper::validate($action['date'],'date'))
                                    break 2;
                                if (!Helper::validate($action['comment'],'text'))
                                    break 2;
                                $updates[$markId] = [
                                    "mark" => $action['mark'],
                                    "createDate" => Helper::formatDateFromGerman($action['date']),
                                    "comment" => $action['comment']
                                ];
                                break;
                            case "DELETE":
                                if (!$this->repository('Marks')->checkMark($markId,$action['studentId'],$action['subjectId']))
                                    break 2;
                                array_push($deletes, $markId);
                                break;
                            case "ADD":
                                if (!Helper::validate($action['mark'],'integer',["minvalue" => 1, "maxvalue" => 6]))
                                    break 2;
                                if (!Helper::validate($action['date'],'date'))
                                    break 2;
                                if (!Helper::validate($action['comment'],'text') && $action['comment'] != "")
                                    break 2;
                                if (!isset($action['isBigMark']) || !Helper::validate($action['isBigMark'], 'integer', ["minvalue" => 0, "maxvalue" => 1]))
                                    break 2;
                                array_push($adds,[
                                    "mark" => $action['mark'],
                                    "createDate" => Helper::formatDateFromGerman($action['date']),
                                    "comment" => $action['comment'],
                                    "yearId" => $this->repository("Settings")->getCurrentYearId(),
                                    "userId" => $action['studentId'],
                                    "subjectId" => $action['subjectId'],
                                    "isBigMark" => $action['isBigMark']
                                ]);

                                break;
                        }
                        $progress++;
                    }
                    if ($progress == count($marks)){
                        //perform updates
                        foreach ($updates as $markId => $update){
                            if (!$this->repository("Marks")->updateMark($markId, $update) && $response != ""){
                                $response = [
                                    'error' => 1,
                                    'code' => $this->container->get('errors')['DATABASE_ERROR']
                                ];
                            };
                        }
                        //perform deletes
                        foreach ($deletes as $markId){
                            if (!$this->repository("Marks")->deleteMark($markId) && $response != ""){
                                $response = [
                                    'error' => 1,
                                    'code' => $this->container->get('errors')['DATABASE_ERROR']
                                ];
                            }
                        }
                        //perform adds
                        foreach ($adds as $add){
                            if (!$this->repository("Marks")->createMark($add) && $response != ""){
                                $response = [
                                    'error' => 1,
                                    'code' => $this->container->get('errors')['DATABASE_ERROR']
                                ];
                            }
                        }
                    }else{
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATA_INVALID']
                        ];
                    }
                }else{
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['DATA_INVALID']
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }
}
?>