<?php
/**
 * Created by PhpStorm.
 * User: lbram
 * Date: 13.12.2016
 * Time: 12:32
 */

namespace src\Controller;


class Subjects extends Controller
{
    public function getSubjectsAction(){
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $subjects = $this->repository('Subjects')->getSubjects();
                foreach ($subjects as $subject) {
                    $response[] = [
                        'id' => $subject['id'],
                        'name' => $subject['name']
                    ];
                }
                $response = [
                    'type' => $cuser['type'],
                    'subjects' => $response
                ];
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }
}