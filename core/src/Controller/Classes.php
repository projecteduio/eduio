<?php
namespace src\Controller;


use src\Library\Helper;

class Classes extends Controller {

    public function getClassesAction(){
        $params = $this->request->getParsedBody();
        
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null || $cuser['type'] == 2 ){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                if ($cuser['type'] == 0 || $cuser['type'] == 3 ){ // user is admin or principal-> retrieve all classes
                    $classes = $this->repository('Classes')->getClasses();
                }else{ // user is teacher -> retrieve all classes that he is allowed to see
                    $classes = $this->repository('Classes')->getClassesOfTeacher($cuser['id']);
                }
                foreach ($classes as $class) {
                    $teacher = $this->repository('Classes')->getClassTeacherOfClass($class['id']);
                    $studentsCount = $this->repository('Classes')->getStudentsCountOfClass($class['id']);
                    if ($teacher !== false){
                        $response[] = [
                            'id' => $class['id'],
                            'name' => $class['displayName'],
                            'classTeacherId' => $teacher['id'],
                            'firstname' => $teacher['firstname'],
                            'lastname' => $teacher['lastname'],
                            'countStudents' => $studentsCount
                        ];
                    }else{
                        $response[] = [
                            'id' => $class['id'],
                            'name' => $class['displayName'],
                            'classTeacherId' => "",
                            'firstname' => "",
                            'lastname' => "",
                            'countStudents' => $studentsCount
                        ];
                    }
                }
                $response = [
                    'type' => $cuser['type'],
                    'classes' => $response
                ];
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function getClassAction(){
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $allow = false;
                $class = $this->repository('Classes')->getClassById($this->args['classId']);
                if ($class != null){
                    if ($cuser['type'] == 0 || $cuser['type'] == 3){ //user is admin or principal -> is allowed to view the class
                        $allow = true;
                    }else if ($cuser['type'] == 1){ // user is teacher -> check if he is a teacher of this class
                        $isTeacherOfClass = false;
                        $classesOfTeacher = $this->repository('Classes')->getClassesOfTeacher($cuser['id']);
                        foreach ($classesOfTeacher as $cOT){
                            if ($cOT['id'] == $class['id']){
                                $isTeacherOfClass = true;
                                break;
                            }
                        }
                        //check if he is the class teacher of this class
                        $c = $this->repository('Classes')->getClassOfClassTeacher($params['userid']);
                        if ($c !== false && $c['id'] == $class['id']) {
                            $isTeacherOfClass = true;
                        }
                        if ($isTeacherOfClass){
                            $allow = true;
                        }
                    }else { // user is a student -> check if he is part of this class
                        $classOfUser = $this->repository('Classes')->getClassOfUser($cuser['id']);
                        if ($classOfUser['id'] == $class['id']){
                            $allow = true;
                        }
                    }
                    if ($allow) {
                        $teacher = $this->repository('Classes')->getClassTeacherOfClass($class['id']);
                        $studentsCount = $this->repository('Classes')->getStudentsCountOfClass($class['id']);
                        if ($teacher !== false) {
                            $response = [
                                'id' => $class['id'],
                                'name' => $class['displayName'],
                                'classTeacherId' => $teacher['id'],
                                'firstname' => $teacher['firstname'],
                                'lastname' => $teacher['lastname'],
                                'countStudents' => $studentsCount
                            ];
                        } else {
                            $response = [
                                'id' => $class['id'],
                                'name' => $class['displayName'],
                                'classTeacherId' => "",
                                'firstname' => "",
                                'lastname' => "",
                                'countStudents' => $studentsCount
                            ];
                        }
                        $response = [
                            'class' => $response
                        ];
                    }else{
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['ACCESS_DENIED']
                        ];
                    }
                }else{
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_CLASS_ID']
                    ];
                }
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function createClassAction(){
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $dataIsInvalid = false;
                if(!isset($params['displayName']) || !Helper::validate($params['displayName'],"text",['minlength' => 1, 'maxlength' => 30])){
                    $dataIsInvalid = true;
                }
                if(isset($params['classTeacherId']) && !Helper::validate($params['classTeacherId'],"integer") && $params['classTeacherId'] !== "NULL"){
                    $dataIsInvalid = true;
                }else if (isset($params['classTeacherId']) && $params['classTeacherId'] !== "NULL"){
                    $classTeacher = $this->repository('User')->getUserById($params['classTeacherId']);
                    if ($classTeacher === false || $classTeacher['type'] != 1){
                        $dataIsInvalid = true;
                    }
                }
                if ($dataIsInvalid) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['DATA_INVALID']
                    ];
                }else {
                    $data = [
                        'displayName' => $params['displayName']
                    ];
                    if (isset($params['classTeacherId'])){
                        $data['classTeacher'] = $params['classTeacherId'] === "NULL" ? null : $params['classTeacherId'];
                    }
                    $result = $this->repository("Classes")->createClass($data);
                    if ($result !== false) {
                        $response = [
                            'resourceId' => $result
                        ];
                    }else{
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATABASE_ERROR']
                        ];
                    }
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function updateClassAction(){
        $params = $this->request->getParsedBody();
        $classId = $this->args['classId'];
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $class = $this->repository('Classes')->getClassById($classId);
                if ($class === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_CLASS_ID']
                    ];
                } else {
                    $dataIsInvalid = false;
                    if(!isset($params['displayName']) || !Helper::validate($params['displayName'],"text",['minlength' => 1, 'maxlength' => 30])){
                        $dataIsInvalid = true;
                    }
                    if(isset($params['classTeacherId']) && !Helper::validate($params['classTeacherId'],"integer") && $params['classTeacherId'] !== "NULL"){
                        $dataIsInvalid = true;
                    }else if (isset($params['classTeacherId']) && $params['classTeacherId'] != "NULL"){
                        $classTeacher = $this->repository('User')->getUserById($params['classTeacherId']);
                        if ($classTeacher === false || $classTeacher['type'] != 1){
                            $dataIsInvalid = true;
                        }
                    }
                    if ($dataIsInvalid) {
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATA_INVALID']
                        ];
                    }else {
                        $data = [
                            'displayName' => $params['displayName']
                        ];
                        if (isset($params['classTeacherId'])){
                            $data['classTeacher'] = $params['classTeacherId'] === "NULL" ? null : $params['classTeacherId'];
                        }
                        $result = $this->repository("Classes")->updateClass($classId, $data);
                        if (!$result) {
                            $response = [
                                'error' => 1,
                                'code' => $this->container->get('errors')['DATABASE_ERROR']
                            ];
                        }
                    }
                }
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function deleteClassAction(){
        $params = $this->request->getParsedBody();
        $classId = $this->args['classId'];
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $class = $this->repository('Classes')->getClassById($classId);
                if ($class === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_CLASS_ID']
                    ];
                } else {
                    $result = $this->repository("Classes")->deleteClass($classId);
                    if (!$result) {
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATABASE_ERROR']
                        ];
                    }
                }
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function getSubjectsAction(){
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null || ($cuser['type'] != 1 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $subjects = $this->repository('Classes')->getSubjectsOfClass($this->args['classId']);
                foreach ($subjects as $subject) {
                    $response[] = [
                        'uuid' => $subject['id'],
                        'name' => $subject['name']
                    ];
                }
                $response = [
                    'type' => $cuser['type'],
                    'classId' => $this->args['classId'],
                    'subjects' => $response
                ];
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function getStudentsOfClassAction(){
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $class = $this->repository('Classes')->getClassById($this->args['classId']);
                if ($class != null) {
                    $allow = false;
                    if ($cuser['type'] == 0 || $cuser['type'] == 3) { //user is admin -> is allowed to view the students of this class
                        $allow = true;
                    } else if ($cuser['type'] == 1) { // user is teacher -> check if he is a teacher of this class
                        $isTeacherOfClass = false;
                        $classesOfTeacher = $this->repository('Classes')->getClassesOfTeacher($cuser['id']);
                        foreach ($classesOfTeacher as $cOT) {
                            if ($cOT['id'] == $class['id']) {
                                $isTeacherOfClass = true;
                                break;
                            }
                        }
                        //check if he is the class teacher of this class
                        $c = $this->repository('Classes')->getClassOfClassTeacher($params['userid']);
                        if ($c !== false && $c['id'] == $class['id']) {
                            $isTeacherOfClass = true;
                        }
                        if ($isTeacherOfClass) {
                            $allow = true;
                        }
                    } else { // user is a student -> check if he is part of this class
                        $classOfUser = $this->repository('Classes')->getClassOfUser($cuser['id']);
                        if ($classOfUser['id'] == $class['id']) {
                            $allow = true;
                        }
                    }
                    if ($allow) {
                        $students = $this->repository('Classes')->getStudentsOfClass($class['id']);
                        foreach($students as $student){
                            if ($student['active'] === '1') {
                                $response[] = [
                                    "id" => $student['id'],
                                    "firstname" => $student['firstname'],
                                    "lastname" => $student['lastname'],
                                    "username" => $student['username'],
                                    "gender" => $student['gender'],
                                    "classid" => $student['classid'],
                                    "class" => $student['class']
                                ];
                            }
                        }
                        $response = [
                            "students" => $response
                        ];
                    }else{
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['ACCESS_DENIED']
                        ];
                    }
                }else{
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_CLASS_ID']
                    ];
                }
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

}
?>