<?php
namespace src\Controller;

use src\Library\Helper;

class Login extends Controller {

    public function indexAction () {
        $params = $this->request->getParsedBody();
        $userRep = $this->repository('User');
        $tokenRep = $this->repository('Token');

        $username = isset($params['username']) ? $params['username'] : '';
        $pw = isset($params['password']) ? $params['password'] : '';

        $user = $userRep->getUserByUsername($username);

        if ($user == false){ // user does not exist or db error occured
            $response = [
                'error' => 1,
                'code' => $this->container->get('errors')['UNKNOWN_USERNAME']
            ];
        }else{ // user exists -> check password
            if (password_verify($pw,$user['password'])){
                $response = [
                    'token' => $tokenRep->generateToken($user['id']),
                    'userid' => $user['id']
                ];

            }else{
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['INVALID_PASSWORD']
                ];
            }
        }

        return $this->render($response);
    }

}