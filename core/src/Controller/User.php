<?php
namespace src\Controller;


use src\Library\Helper;

class User extends Controller {

    public function getUsersAction () {
        $body = $this->request->getParsedBody();
        try {
            $cuser = isset($body['userid']) ? $this->repository('User')->getUserById($body['userid']) : null;
            $response = [];
            if ($cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $users = $this->repository('User')->getUsers(true);
                foreach ($users as $user) {
                    $response[] = [
                        'uuid' => $user['id'],
                        'gender' => $user['gender'],
                        'firstname' => $user['firstname'],
                        'lastname' => $user['lastname'],
                        'type' => $user['type'],
                        'active' => $user['active'],
                        'class' => isset($user['class']) ? $user['class'] : "",
                        'classid' => isset($user['classid']) ? $user['classid'] : ""
                    ];
                }
                $response = [
                    'users' => $response
                ];
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function getUserAction(){
        $body = $this->request->getParsedBody();
        try {
            $cuser = isset($body['userid']) ? $this->repository('User')->getUserById($body['userid']) : null;
            if ($cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $userData = $this->repository('User')->getUserById($this->args['userId']);
                if ($userData === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_USER_ID']
                    ];
                } else {
                    $user = [
                        'uuid' => $userData['id'],
                        'gender' => $userData['gender'],
                        'firstname' => $userData['firstname'],
                        'lastname' => $userData['lastname'],
                        'username' => $userData['username'],
                        'type' => $userData['type'],
                        'active' => $userData['active'],
                        'class' => isset($userData['class']) ? $userData['class'] : "",
                        'classId' => isset($userData['classId']) ? $userData['classId'] : ""
                    ];

                    $response = [
                        'user' => $user
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function deleteUsersAction(){
        $body = $this->request->getParsedBody();
        try{
            $cuser = isset($body['userid']) ? $this->repository('User')->getUserById($body['userid']) : null;
            $response = "";
            if ($cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $dataIsInvalid = false;
                if (isset($body['users']) && count($body['users']) != 0){
                    $users = $body['users'];
                    foreach ($users as $user){
                        if (!Helper::validate($user,'integer')) {
                            $dataIsInvalid = true;
                            break;
                        }
                    }
                }else{
                    $dataIsInvalid = true;
                    $message = "no users specified";
                }

                if ($dataIsInvalid) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['DATA_INVALID'],
                        'message' => $message
                    ];
                }else{
                    $result = $this->repository('User')->deleteUsers($users);
                    if (!$result){
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATABASE_ERROR']
                        ];
                    }
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }


    /**
     * needed post data
     *  userid, firstname, lastname, type, active
     */
    public function updateUserAction(){
        $body = $this->request->getParsedBody();
        try {
            $cuser = isset($body['userid']) ? $this->repository('User')->getUserById($body['userid']) : null;
            $response = "";
            if ($cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $user = $this->repository('User')->getUserById($this->args['userId']);
                if ($user === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_USER_ID']
                    ];
                } else {
                    $data['id'] = $this->args['userId'];
                    $dataIsInvalid = false;
                    $message = "";
                    if (isset($body['firstname']) && !Helper::validate($body['firstname'], 'text', ['minlength' => 2, 'maxlength' => 50])){
                        $dataIsInvalid = true;
                        $message = "firstname invalid";
                    }else if (isset($body['firstname'])){
                        $data['firstname'] = $body['firstname'];
                    }

                    if (isset($body['lastname']) && !Helper::validate($body['lastname'],'text',['minlength' => 2, 'maxlength' => 50])) {
                        $dataIsInvalid = true;
                        $message = "lastname invalid";
                    } else if (isset($body['lastname'])){
                        $data['lastname'] = $body['lastname'];
                    }

                    if (isset($body['type']) && !Helper::validate($body['type'],'integer',['minvalue' => 0, 'maxvalue' => 3])) {
                        $dataIsInvalid = true;
                        $message = "type invalid";
                    } else if (isset($body['type'])){
                        $data['type'] = $body['type'];
                    }

                    if (isset($body['active']) && !Helper::validate($body['active'],'integer',['minvalue' => 0, 'maxvalue' => 1])) {
                        $dataIsInvalid = true;
                        $message = "active invalid";
                    } else if (isset($body['active'])){
                        $data['active'] = $body['active'];
                    }

                    if (isset($body['gender']) && !Helper::validate($body['gender'],'integer',['minvalue' => 0, 'maxvalue' => 1])) {
                        $dataIsInvalid = true;
                        $message = "gender invalid";
                    } else if (isset($body['gender'])){
                        $data['gender'] = $body['gender'];
                    }

                    if (isset($body['password']) && !Helper::validate($body['password'],'text',['minlength' => 8])) {
                        $dataIsInvalid = true;
                        $message = "password invalid";
                    } else if (isset($body['password'])){
                        $data['password'] = password_hash($body['password'], PASSWORD_DEFAULT);
                    }

                    if (isset($body['class']) && $this->repository("Classes")->getClassById($body['class']) == false) {
                        $dataIsInvalid = true;
                        $message = "class invalid";
                    } else if (isset($body['class'])){
                        $data['class'] = $body['class'];
                    }

                    if ($dataIsInvalid) {
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATA_INVALID'],
                            'message' => $message
                        ];
                    }else{
                        $result = $this->repository("User")->updateUser($data);
                        if ($result !== false && isset($data['class']) && $data['type'] == "2"){
                            $result = $this->repository("User")->updateClassOfUser($this->args['userId'],$data['class']);
                        }else if($result !== false && $data['type'] != "2"){
                            $result = $this->repository("User")->removeClassOfUser($this->args['userId']);
                        }
                        if (!$result){
                            $response = [
                                'error' => 1,
                                'code' => $this->container->get('errors')['DATABASE_ERROR']
                            ];
                        }
                    }
                }
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function createUserAction(){
        $body = $this->request->getParsedBody();
        try{
            $cuser = isset($body['userid']) ? $this->repository('User')->getUserById($body['userid']) : null;
            $response = "";
            if ($cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $dataIsInvalid = false;
                $message = "";
                if (!isset($body['firstname']) || !Helper::validate($body['firstname'], 'text', ['minlength' => 2, 'maxlength' => 50])){
                    $dataIsInvalid = true;
                    $message = "firstname invalid";
                }
                if (!isset($body['lastname']) || !Helper::validate($body['lastname'],'text',['minlength' => 2, 'maxlength' => 50])) {
                    $dataIsInvalid = true;
                    $message = "lastname invalid";
                }
                if (!isset($body['type']) || !Helper::validate($body['type'],'integer',['minvalue' => 0, 'maxvalue' => 3])) {
                    $dataIsInvalid = true;
                    $message = "type invalid";
                }else if ($body['type'] == "2" && (!isset($body['class']) || $this->repository("Classes")->getClassById($body['class']) == false)){
                    $dataIsInvalid = true;
                    $message = "unknown class id";
                }
                if (!isset($body['active']) || !Helper::validate($body['active'],'integer',['minvalue' => 0, 'maxvalue' => 1])) {
                    $dataIsInvalid = true;
                    $message = "active invalid";
                }
                if (!isset($body['gender']) || !Helper::validate($body['gender'],'integer',['minvalue' => 0, 'maxvalue' => 1])) {
                    $dataIsInvalid = true;
                    $message = "gender invalid";
                }
                if (!isset($body['password']) || !Helper::validate($body['password'],'text',['minlength' => 8])) {
                    $dataIsInvalid = true;
                    $message = "password invalid";
                }
                if ($dataIsInvalid) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['DATA_INVALID'],
                        'message' => $message
                    ];
                }else{
                    $data['firstname'] = $body['firstname'];
                    $data['lastname'] = $body['lastname'];
                    $data['username'] = $this->repository('User')->generateUsername($data['firstname'], $data['lastname']);
                    $data['type'] = $body['type'];
                    $data['active'] = $body['active'];
                    $data['gender'] = $body['gender'];
                    $data['password'] = password_hash($body['password'], PASSWORD_DEFAULT);
                    $result = $this->repository("User")->createUser($data);
                    $userId = "";
                    if ($result !== false){
                        $userId = $result;
                        if ($data['type'] == "2"){
                            $result = $this->repository("User")->updateClassOfUser($userId,$body['class']);
                        }
                    }
                    if ($result !== false){
                        $response = [
                            'resourceId' => $userId
                        ];
                    }else{
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATABASE_ERROR']
                        ];
                    }
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function deleteUserAction(){
        $body = $this->request->getParsedBody();
        try{
            $cuser = isset($body['userid']) ? $this->repository('User')->getUserById($body['userid']) : null;
            $response = "";
            if ($cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $user = $this->repository('User')->getUserById($this->args['userId']);
                if ($user === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_USER_ID']
                    ];
                } else {
                    $result = $this->repository('User')->deleteUser($this->args['userId']);
                    if (!$result){
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATABASE_ERROR']
                        ];
                    }
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function getPermissionsAction(){
        $body = $this->request->getParsedBody();
        try {
            $cuser = isset($body['userid']) ? $this->repository('User')->getUserById($body['userid']) : null;
            if ($cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $permissionsData = $this->repository('User')->getPermissionsOfUser($this->args['userId']);
                if ($permissionsData === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_USER_ID']
                    ];
                } else {
                    $permissions = array();
                    if ($permissionsData !== false) {
                        foreach ($permissionsData as $permission) {
                            $permissions[] = [
                                'id' => $permission['id'],
                                'subjectId' => $permission['subjectId'],
                                'classId' => $permission['classId'],
                                'access' => $permission['access'],
                            ];
                        }
                    }
                    $response = [
                        'permissions' => $permissions,
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function updatePermissionsAction(){
        $body = $this->request->getParsedBody();
        try {
            $cuser = isset($body['userid']) ? $this->repository('User')->getUserById($body['userid']) : null;
            $response = "";
            if ($cuser == null || ($cuser['type'] != 0 && $cuser['type'] != 3)){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $user = $this->repository('User')->getUserById($this->args['userId']);
                if ($user === false) {
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_USER_ID']
                    ];
                } else {
                    $data['userId'] = $this->args['userId'];
                    $dataIsInvalid = false;

                    $permissions = $body['permissions'];
                    $i=0;
                    foreach($permissions as $permission){
                        if (!isset($permission['subjectId']) || $this->repository('Subjects')->getSubjectById($permission['subjectId']) == null){
                            $dataIsInvalid = true;
                            break;
                        }
                        if (!isset($permission['classId']) || $this->repository('Classes')->getClassById($permission['classId']) == null){
                            $dataIsInvalid = true;
                            break;
                        }
                        if (!isset($permission['access']) || !Helper::validate($permission['access'],'integer',['minvalue' => 0, 'maxvalue' => 1])){
                            $dataIsInvalid = true;
                            break;
                        }
                    }
                    $permissionKeys = [];
                    foreach($permissions as $permission){
                        $permissionKey = $permission['classId']."|".$permission["subjectId"];
                        if (in_array($permissionKey,$permissionKeys)){
                            $dataIsInvalid= true; //because duplicate permission
                            break;
                        }
                        array_push($permissionKeys,$permissionKey);
                    }

                    if ($dataIsInvalid) {
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATA_INVALID']
                        ];
                    }else{
                        $result = $this->repository("User")->updatePermissionsOfUser($this->args['userId'], $permissions);
                        if (!$result){
                            $response = [
                                'error' => 1,
                                'code' => $this->container->get('errors')['DATABASE_ERROR']
                            ];
                        }
                    }
                }
            }

        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }


    public function updatePasswordAction(){
        $body = $this->request->getParsedBody();
        try {
            $cuser = isset($body['userid']) ? $this->repository('User')->getUserById($body['userid']) : null; //logged in user
            $user = $this->repository('User')->getUserById($this->args['userId']);// user whose password should be updated
            $response = "";
            if ($cuser == null || $user['id'] !== $cuser['id']){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $newPassword = isset($body['newpassword']) ? $body['newpassword'] : "";
                $oldPassword = isset($body['oldpassword']) ? $body['oldpassword'] : "";
                if (password_verify($oldPassword,$user['password'])){
                    if (!Helper::validate($newPassword,'text',['minlength' => 8])){
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATA_INVALID']
                        ];
                    }else {
                        $data = [
                            "id" => $user['id'],
                            "password" => password_hash($newPassword, PASSWORD_DEFAULT)
                        ];
                        $result = $this->repository("User")->updateUser($data);
                        if (!$result) {
                            $response = [
                                'error' => 1,
                                'code' => $this->container->get('errors')['DATABASE_ERROR']
                            ];
                        }
                    }
                }else{
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['INVALID_PASSWORD']
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }
}
?>