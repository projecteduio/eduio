<?php
namespace src\Controller\Dashboard;

use src\Controller\Controller;

class Dashboard extends Controller
{
    public function indexAction () {
        return $this->render(['data' => 'test']);
    }

}