<?php
namespace src\Controller;

use src\Library\Helper;

class Years extends Controller {

    public function getCurrentYearAction () {
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $currentYear = $this->repository("Years")->getCurrentYear();
                $response = [
                    "year" => $currentYear
                ];
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function setCurrentYearAction () {
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null || $cuser['type'] != 3){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $year = isset($params['year']) ? $this->repository("Years")->getYearById($params['year']) : null;
                if ($year !== false && $year != null){
                    $result = $this->repository("Years")->setCurrentYear($params['year']);
                    if (!$result){
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATABASE_ERROR']
                        ];
                    }
                }else{
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_YEAR_ID']
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function getYearsAction () {
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else{
                $years = $this->repository("Years")->getYears();
                $response = [
                    "years" => $years
                ];
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function updateYearAction(){
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null || $cuser['type'] != 3){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $year = $this->repository("Years")->getYearById($this->args['yearId']);
                if ($year !== false){
                    if (isset($params['displayName']) && Helper::validate($params['displayName']) && $params['displayName'] != ""){
                        $result = $this->repository("Years")->updateYear($year['id'],$params['displayName']);
                        if (!$result){
                            $response = [
                                'error' => 1,
                                'code' => $this->container->get('errors')['DATABASE_ERROR']
                            ];
                        }
                    }else{
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATA_INVALID']
                        ];
                    }
                }else{
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_YEAR_ID']
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function deleteYearAction(){
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null || $cuser['type'] != 3){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                $year = $this->repository("Years")->getYearById($this->args['yearId']);
                if ($year !== false){
                    $result = $this->repository("Years")->deleteYear($this->args['yearId']);
                    if (!$result){
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATABASE_ERROR']
                        ];
                    }
                }else{
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['UNKNOWN_YEAR_ID']
                    ];
                }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }

    public function createYearAction(){
        $params = $this->request->getParsedBody();
        try {
            $cuser = isset($params['userid']) ? $this->repository('User')->getUserById($params['userid']) : null;
            $response = [];
            if ($cuser === false || $cuser == null || $cuser['type'] != 3){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['ACCESS_DENIED']
                ];
            }else {
                if (isset($params['displayName']) && Helper::validate($params['displayName']) && $params['displayName'] != ""){
                    $result = $this->repository("Years")->createYear($params['displayName']);
                    if (!$result){
                        $response = [
                            'error' => 1,
                            'code' => $this->container->get('errors')['DATABASE_ERROR']
                        ];
                    }else{
                        $response = [
                            'resourceId' => $result
                        ];
                    }
                }else{
                    $response = [
                        'error' => 1,
                        'code' => $this->container->get('errors')['DATA_INVALID']
                    ];
                    }
            }
        }catch (\PDOException $e){
            $response = ['error' => 1, 'code' => $this->container->get('errors')['DATABASE_ERROR']];
            if ($this->container['debug']){
                echo $e->getMessage();
            }
        }
        return $this->render($response);
    }
}