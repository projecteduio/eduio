<?php
namespace src\Controller;

use src\Library\Helper;

class Layout extends Controller {

    public function indexAction () {
        $params = $this->request->getParsedBody();
        $tokenRep = $this->repository('Token');

        $token = isset($params['token']) ? $params['token'] : '';
        $userId = isset($params['userid']) ? $params['userid'] : '';

        $valid = $tokenRep->checkToken($token,$userId);

        if ($valid){
            $cuser = $this->repository('User')->getUserById($params['userid']);
            $response = [
                'token' => $tokenRep->generateToken($userId),
                'userid' => $userId,
                'type' => $cuser['type'],
                'name' => $cuser['firstname'] . " " . $cuser['lastname'],
                'classid' => ""
            ];
            if ($cuser['type'] == 1){
                $class = $this->repository('Classes')->getClassOfClassTeacher($params['userid']);
                if ($class !== false) {
                    $response['classid']= $class['id'];
                }
            }
        }else{
            if ($token == ''){
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['TOKEN_NOT_SPECIFIED'],
                    'logout' => 1
                ];
            }else{
                $response = [
                    'error' => 1,
                    'code' => $this->container->get('errors')['INVALID_TOKEN'],
                    'logout' => 1
                ];
            }
        }

        return $this->render($response);
    }

}