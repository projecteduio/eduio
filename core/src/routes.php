<?php

use src\Middleware\Authentification;

//Layout Route
$app->post('/layout', $cN.'Layout:index')->setName('layout');
$app->post('/login', $cN.'Login:index')->setName('login');

$app->group('/', function () use ($app,$cN){
    $app->group('users', function () use ($app, $cN) {
        $app->post('', $cN . 'User:getUsers')->setName('getUsers');
        $app->delete('', $cN . 'User:deleteUsers')->setName('deleteUsers');
        $app->post('/create', $cN . 'User:createUser')->setName('createUser');
        $app->group('/{userId}', function () use ($app, $cN) {
            $app->put('', $cN . 'User:updateUser')->setName('updateUser');
            $app->delete('', $cN . 'User:deleteUser')->setName('deleteUser');
            $app->post('', $cN . 'User:getUser')->setName('getUser');
            $app->put('/password', $cN . 'User:updatePassword')->setName('updatePassword');
            $app->group('/marks', function () use ($app, $cN) {
                $app->post('', $cN . 'Marks:getMarksOfUser')->setName('getMarksOfUser');
            });
            $app->group('/permissions', function () use ($app, $cN){
                $app->post('', $cN . 'User:getPermissions')->setName('getPermissionsOfUser');
                $app->put('', $cN . 'User:updatePermissions')->setName('updatePermissionsOfUser');
            });
        });
    });
    $app->group('settings', function () use ($app, $cN) {
        $app->group('/years', function () use ($app, $cN) {
            $app->post('', $cN . 'Years:getYears')->setName('getYears');
            $app->post('/current', $cN . 'Years:getCurrentYear')->setName('getCurrentYear');
            $app->put('/current', $cN . 'Years:setCurrentYear')->setName('setCurrentYear');
            $app->post('/create', $cN . 'Years:createYear')->setName('createYear');
            $app->group('/{yearId}', function () use ($app, $cN) {
                $app->put('', $cN . 'Years:updateYear')->setName('updateYear');
                $app->delete('', $cN . 'Years:deleteYear')->setName('deleteYear');
            });
        });
    });
    $app->group('classes', function () use ($app, $cN) {
        $app->post('', $cN . 'Classes:getClasses')->setName('getClasses');
        $app->post('/create', $cN . 'Classes:createClass')->setName('createClass');
        $app->group('/{classId}', function () use ($app, $cN) {
            $app->post('/students', $cN . 'Classes:getStudentsOfClass')->setName('getStudentsOfClass');
            $app->post('', $cN . 'Classes:getClass')->setName('getClass');
            $app->delete('', $cN . 'Classes:deleteClass')->setName('deleteClass');
            $app->put('', $cN . 'Classes:updateClass')->setName('updateClass');
            $app->group('/subjects',  function () use ($app, $cN) {
                $app->post('', $cN . 'Classes:getSubjects')->setName('getSubjectsOfClass');
                $app->group('/{subjectId}/marks', function () use ($app, $cN) {
                    $app->post('', $cN . 'Marks:getMarksOfSubject')->setName('getMarksOfSubject');
                    $app->put('', $cN . 'Marks:updateMarksOfSubject')->setName('updateMarksOfSubject');
                    $app->post('/create', $cN . 'Marks:addMarksOfSubject')->setName('addMarksOfSubject');
                    $app->delete('', $cN . 'Marks:deleteMarksOfSubject')->setName('deleteMarksOfSubject');
                });
            });
        });
    });
    $app->group('subjects', function () use ($app, $cN) {
        $app->post('', $cN . 'Subjects:getSubjects')->setName('getSubjects');
    });
})->add(new Authentification($app->getContainer()));
// });

$app->post('/test',function (\Slim\Http\Request $request, \Slim\Http\Response $response) {

    return $response;
})->add(new Authentification($app->getContainer()));

// classes/2/subjects/3/marks