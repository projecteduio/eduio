<?php
namespace src\Library;

class RepositoryFactory {

    private $eduioDB;

    private $container;

    public function __construct ($eduioDB,$container) {
        $this->eduioDb = $eduioDB;
        $this->container = $container;
    }

    public function loadRepository ($name) {
        $repositoryName = 'src\Repository\\' . $name;
        return new $repositoryName($this->eduioDb, $this->container);
    }
}