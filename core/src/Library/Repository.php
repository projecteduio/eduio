<?php
namespace src\Library;

class Repository {

    protected $eduioDB;

    protected $container;

    public function __construct ($eduioDB, $container) {
        $this->eduioDB = $eduioDB;
        $this->container = $container;
    }
}