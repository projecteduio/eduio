/**
 * Class: RestService
 *
 * Package: Parlameter
 *
 * extends <Coco.View>
 *
 * Description:
 *
 * (c) 2013 3m5. Media GmbH
 */
'use strict';
var Coco = require('3m5-coco');

var RestService = dejavu.Class.declare({
    $name: 'RestService',

    $extends: Coco.BaseRestService,

    //unique service id
    $serviceId: 'RestService',

    _restServicePath: '/',

    getId() {
        return this.$name;
    },

    getLayout(callbackSuccess) {
        var mprogress = new Mprogress();
        mprogress.start();
        return this._post(
            'layout',
            [],
            {
                userid: $.jStorage.get('userid'),
                token: $.jStorage.get('token')
            },
            this.xhrFields,
            function (response) {
                console.log(response);
                mprogress.end();
                if (callbackSuccess) {
                    $.jStorage.set('token', response.token);
                    $.jStorage.set('userid', response.userid);
                    $.jStorage.set('usertype', response.type);
                    $.jStorage.set('classid', response.classid);
                    callbackSuccess(response);
                }
            },
            function (response) {
                console.log('error', response);
                mprogress.end();
            },
            'application/x-www-form-urlencoded; charset=UTF-8'
        );
    },
    Login(data, callbackSuccess) {
        var mprogress = new Mprogress();
        mprogress.start();
        return this._post(
            'login',
            [],
            data,
            this.xhrFields,
            function (response) {
                console.log(response);
                mprogress.end();
                if (callbackSuccess) {
                    callbackSuccess(response);
                }
            },
            function (response) {
                console.log('error', response);
                mprogress.end();
            },
            'application/x-www-form-urlencoded; charset=UTF-8'
        );
    },
    logout(response){
        if(typeof response.token=='undefined'){
            location.reload();
        }

    },
    getResource(page, data, callbackSuccess){
        var mprogress = new Mprogress();
        mprogress.start();
        if (data!="") {
            data.userid = $.jStorage.get('userid');
            data.token = $.jStorage.get('token');
        }else{
            var data = {
                userid: $.jStorage.get('userid'),
                token: $.jStorage.get('token')
            };
        }

        return this._post(
            page,
            [],
            data,
            this.xhrFields,
            (response) => {
                console.log(response);
                mprogress.end();
                if (callbackSuccess) {
                    this.logout(response);
                    $.jStorage.set('token', response.token);
                    $.jStorage.set('userid', response.userid);
                    callbackSuccess(response);
                }
            },
            function (response) {
                console.log('error', response);
                mprogress.end();
            },
            'application/x-www-form-urlencoded; charset=UTF-8'
        );
    },
    deleteResource(page, data, callbackSuccess){
        var mprogress = new Mprogress();
        mprogress.start();
        if (data!="") {
            data.userid = $.jStorage.get('userid');
            data.token = $.jStorage.get('token');
        }else{
            var data = {
                userid: $.jStorage.get('userid'),
                token: $.jStorage.get('token')
            };
        }

        return this._delete(
            page,
            [],
            data,
            this.xhrFields,
            (response) => {
                console.log(response);
                mprogress.end();
                if (callbackSuccess) {
                    this.logout(response);
                    $.jStorage.set('token', response.token);
                    $.jStorage.set('userid', response.userid);
                    callbackSuccess(response);
                }
            },
            function (response) {
                console.log('error', response);
                mprogress.end();
            },
            'application/x-www-form-urlencoded; charset=UTF-8'
        );
    },

    updateResource(page, data, callbackSuccess){
        var mprogress = new Mprogress();
        mprogress.start();
        if (data!="") {
            data.userid = $.jStorage.get('userid');
            data.token = $.jStorage.get('token');
        }else{
            var data = {
                userid: $.jStorage.get('userid'),
                token: $.jStorage.get('token')
            };
        }

        return this._put(
            page,
            [],
            data,
            this.xhrFields,
            (response) => {
                mprogress.end();
                if (callbackSuccess) {
                    this.logout(response);
                    $.jStorage.set('token', response.token);
                    $.jStorage.set('userid', response.userid);
                    callbackSuccess(response);
                }
            },
            function (response) {
                console.log('error', response);
                mprogress.end();
            },
            'application/x-www-form-urlencoded; charset=UTF-8'
        );
    },

    createResource(page, data, callbackSuccess){
        return this.getResource(page, data, callbackSuccess);
    }
});

module.exports = new RestService();