var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'studentsView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
        'click tr.link': 'openStudent'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/classes/students.hbs'),

    _onInitialize() {

    },
    onActive: function(id){

        var model = new Coco.Model();
        this.setModel(model);
        var progress = 0;
        var ready = 2;

        this._getService('RestService').getResource('classes/'+id, '', (response)=>{
            model.set("class",response.class);
            progress++;
            if (progress == ready){
                this.render();
            }
        });

        this._getService('RestService').getResource('classes/'+id+'/students', '', (response)=>{
            model.set("students",response.students);
            progress++;
            if (progress == ready){
                this.render();
            }
        });
    },

    openStudent(event){
        var studentid = $(event.currentTarget).data("studentid");
        this._getRouter().callRoute("student", {id: this.getModel().get("class").id, studentid: studentid});
    }
});