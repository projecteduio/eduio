var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'classesView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
        'click [name=btnUpdateClass]' : 'updateClass'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/classes/editClass.hbs'),

    _onInitialize() {

    },
    onActive: function(id){

        var model = new Coco.Model();
        this.setModel(model);

        var progress = 0;
        var ready = 3;

        this._getService('RestService').getResource('classes/'+id, '', (response)=>{
            model.set("class",response.class);
            progress++;
            if (progress == ready){
                this.render();
            }
        });

        this._getService('RestService').getResource('users', '', (response)=>{
            var users = response.users;
            var teachers = [];
            users.forEach(function(user, index, users){
                if (user.type == 1){
                    teachers.push(user);
                }
            });
            model.set("teachers",teachers);
            progress++;
            if (progress == ready) {
                this.render();
            }
        });

        this._getService('RestService').getResource('classes/'+id+'/students', '', (response)=>{
            model.set("students",response.students);
            progress++;
            if (progress == ready){
                this.render();
            }
        });
    },

    updateClass(event){
        var classId = (this.getModel().get("class")).id;
        var className = $('[name=displayName]').val();

        if (className.length < 2){
            $.mSnackbar(Coco.Translator.get('global.class.validation.class_too_short'));
            return;
        }

        var teacherId = $('[name=classTeacher]').val();
        var data = {
            'displayName': className
        }

        if (teacherId != -1){
            data['classTeacherId']= teacherId;
        }else{
            data['classTeacherId'] = "NULL";
        }

        this._getService('RestService').updateResource("classes/"+classId, data, (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.messages.updateClass'));
                this.onActive(classId);
            }else{
                if (response.code == "9"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dataInvalid'));
                }else if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }
        });
    }
});