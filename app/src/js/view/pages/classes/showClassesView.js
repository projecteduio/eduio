var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'classesView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/classes/showClasses.hbs'),

    _onInitialize() {

    },
    onActive: function(){
        this._getService('RestService').getResource('classes', '', (response)=>{
            this.setModel(new Coco.Model(response));
            this.render();
        });
    },
});