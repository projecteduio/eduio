var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'editClassesView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
        'click td.link' : 'editClass',
        'click [data-id=btnDeleteClass]' : 'showDeleteClassModal',
        'click #deleteClassModalCancel' : 'cancelDeleteClass',
        'click #deleteClassModalAction' : 'deleteClass',
        'click button[data-id="searchOpen"]': 'openSearch',
        'click button[data-id="searchClose"]' : 'closeSearch',
        'click input[type="checkbox"]' : 'toggletr',
        'click input[type="checkbox"].all' : 'toggleall',
        'keyup [data-id="materialTableSearch"]' : 'searchClass'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/classes/editClasses.hbs'),

    _onInitialize() {

    },
    onActive: function(){
        this._getService('RestService').getResource('classes', '', (response)=>{
            this.setModel(new Coco.Model(response));
            this.render();
        });
    },

    editClass(event){
        var classId = $(event.currentTarget).parent().attr("data-id");
        console.log(classId);
        this._getRouter().callRoute("classEdit", {id: classId});
    },

    showDeleteClassModal(event){
        var classId = $(event.currentTarget).parent().parent().attr("data-id");
        $("#deleteClassModal").attr('data-id',classId);
        $("#deleteClassModal").show();
    },

    cancelDeleteClass(){
        $("#deleteClassModal").hide();
    },

    deleteClass(event){
        var classId = $("#deleteClassModal").attr("data-id");
        console.log(classId);
        this._getService('RestService').deleteResource("classes/" + classId, '', (response) => {
            if (!response.hasOwnProperty('error')) {
                $.mSnackbar(Coco.Translator.get('global.messages.deleteClass'));
                this.onActive();
            } else {
                if (response.code == "7") {
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                } else if (response.code == "1") {
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                } else {
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }

        });
    },

    openSearch() {
        this.$el.find('.materialTitle').hide();
        this.$el.find('.materialSearch').show();
        this.$el.find('.materialSearch input').val('');
        this.$el.find('.materialSearch input').focus();
    },

    closeSearch(){
        this.$el.find('.materialSearch').hide();
        this.$el.find('.materialTitle').show();
    },
    searchClass(event){
        if(!$('.errorFound').hasClass('hide')){
            $('.errorFound').addClass('hide');
        }
        if($(event.currentTarget).val()==""){
            $('tr.link').removeClass('hide');
        }else{
            $('tr.link').addClass('hide');
            $('tr.link:contains('+$(event.currentTarget).val()+')').removeClass('hide');
            if($('tr.link').not('.hide').length==0){
                $('.errorFound').removeClass('hide');
            }
        }

    },
    openCheckboxCounter(){
        this.$el.find('.materialTitle').hide();
        this.$el.find('.materialSelect').show();
    },

    toggletr(event){
        if($(event.currentTarget).parents('tr').hasClass('active')){
            $(event.currentTarget).parents('tr').removeClass('active');
        }
        else{
            $(event.currentTarget).parents('tr').addClass('active');
        }
        this.updateSelect();
    },

    toggleall(event){
        if($(event.currentTarget).attr('data-active') == 1) {
            $(event.currentTarget).attr('data-active', 0);
            this.$el.find('tbody tr.active input[type="checkbox"]').trigger('click');
            this.$el.find('tbody input[type="checkbox"]').trigger('click');
            this.$el.find('tbody tr.active input[type="checkbox"]').trigger('click');
        }
        else{
            $(event.currentTarget).attr("data-active", 1);
            this.$el.find('tbody tr.active input[type="checkbox"]').trigger('click');
            this.$el.find('tbody input[type="checkbox"]').trigger('click');
        }

    },

    updateSelect(){
        this.countSelected = this.$el.find('tbody tr.active').length;
        if(this.countSelected > 0){
            this.$el.find('.materialTitle').hide();
            this.$el.find('.materialSelect').show();
            this.$el.find('.materialSelect span').text(this.countSelected);
        }
        else{
            this.$el.find('.materialSelect').hide();
            this.$el.find('.materialTitle').show();
        }
    }
});