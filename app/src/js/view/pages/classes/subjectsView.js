var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'subjectView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/class/index.hbs'),

    _onInitialize() {

    },
    onActive: function(classId){
        this._getService('RestService').getResource('classes/'+classId+'/subjects', '', (response)=>{
            this.setModel(new Coco.Model(response));
            this.render();
        });
    }
});