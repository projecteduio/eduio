var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'classView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    i:0,
    _events: {
        // 'click .edit_marks_disabled' : 'CalcMode',
        // 'click .edit_marks_enabled' : 'closeCalcMode',
        'click .new_marks' : 'toggleNewMarkPopover',
        'input [name="markInput"]' : 'checkMarkData',
        'input [name="dateInput"]' : 'checkMarkData',
        'click [name="deleteMark"]': 'deleteMark',
        'click .marks': 'toggleMarkPopover',
        'click [name="updateMark"]': 'updateMark',
        'click #editMarks': 'startEditing',
        'click #cancelEdit': 'cancelEdit',
        'click #submitMarks': 'submitMarks',
        // 'change .small_marks input.new_marks' : 'calcBigInput',
        //'click .new_small_marks_check' : 'average',


        //'EVENT CSS-Selector': 'eventhandler-function'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/classes/showMarks.hbs'),

    __diff: {},

    __classId: 0,

    __subjectId: 0,

    _onInitialize() {
    },

    onActive: function(classId, subjectId){
        this.__classId = classId;
        this.__subjectId = subjectId;
        var model = new Coco.Model();
        this.setModel(model);
        model.set("subjectId",subjectId);
        var progress = 0;
        var ready = 2;

        this._getService('RestService').getResource('classes/'+classId+'/subjects/'+subjectId+'/marks', '', (response)=>{
            model.set("permission", response.permission);
            model.set("marks",response.marks);
            progress++;
            if (progress == ready){
                this.render();
            }
        });

        this._getService('RestService').getResource('classes/'+classId+'/students', '', (response)=>{
            model.set("students",response.students);
            progress++;
            if (progress == ready){
                this.render();
            }
        });
        this.__diff = {};
    },
    onRender(){
        $(".marks").popover({
            html: true,
            content: function () {
                return $(this).children(".form_edit_mark").html();
            },
            trigger: "manual",
            placement: "bottom"
        });
        $(".new_marks").popover({
            html: true,
            content: function () {
                return $(".form_new_mark").html();
            },
            trigger: "manual",
            placement: "right"
        });
        $("body").tooltip({
            selector: ".marks"
        });
        this.updateAverage();
    },

    render(){
        var students = this.getModel().get("students");
        var marks = this.getModel().get("marks");
        var tmp = {};
        students.forEach(function(student, index, students){
            if (marks[student.id] != undefined){
                tmp[student.id]=marks[student.id];
                tmp[student.id]={
                    "studentId": student.id,
                    "firstname": student.firstname,
                    "lastname": student.lastname,
                    "small": marks[student.id]["small"],
                    "big":marks[student.id]["big"]
                };
            }else{
                tmp[student.id]={
                    "studentId": student.id,
                    "firstname": student.firstname,
                    "lastname": student.lastname,
                    "small": [],
                    "big":[]
                };
            }

        });
        this.getModel().set("marks",tmp);
        this.$super();
        this.onRender();
    },

    updateAverage(){
        var finalMarks= [];
        var studentsCount = 0;
        $(".student").each(function(){
            studentsCount++;
            var student = $(this);
            var bigMarks = [];
            var smallMarks = [];
            var averageSmall = 0;
            var averageBig = 0;

            //retrieve marks
            student.find(".big_marks .marks").each(function(){
                bigMarks.push(parseInt($(this).data("mark")));
            });
            student.find(".small_marks .marks").each(function(){
                smallMarks.push(parseInt($(this).data("mark")));
            });

            // calculate average
            if (smallMarks.length > 0) {
                smallMarks.forEach(function (mark) {
                    averageSmall += mark;
                });
                averageSmall /= smallMarks.length;
            }

            if (bigMarks.length > 0) {
                bigMarks.forEach(function (mark) {
                    averageBig += mark;
                });
                averageBig /= bigMarks.length;
            }

            averageBig = Math.round(averageBig * 10) / 10;
            averageSmall = Math.round(averageSmall * 10) / 10;

            // calculate final mark
            var finalMark = Math.round(((Math.round(averageBig)+Math.round(averageSmall))/2)*10) / 10;

            //print everything to screen
            if (averageSmall != 0){
                $(this).find(".average_small").text(averageSmall);
            }else{
                $(this).find(".average_small").text("");
            }
            if (averageBig != 0){
                $(this).find(".average_big").text(averageBig);
            }else{
                $(this).find(".average_big").text("");
            }
            if (averageBig != 0 && averageSmall != 0){
                $(this).find(".total_mark").text(finalMark);
                finalMarks.push(finalMark);
            }else{
                $(this).find(".total_mark").text("");
            }
        });
        if (finalMarks.length == studentsCount && studentsCount > 0){
            var totalAverage = 0;
            finalMarks.forEach(function(mark){
               totalAverage += mark;
            });
            totalAverage /= studentsCount;
            totalAverage = Math.round(totalAverage*10)/10;
            $(".average_total_marks").text(totalAverage);
        }
    },

    checkMarkData(event){
        if ($(event.currentTarget).is('[name="markInput"')){ //user typed something into mark input field
            var mark = $(event.currentTarget).val();
            if (mark.length > 1){
                mark = mark.charAt(0);
                $(event.currentTarget).val(mark);
            }
            if (mark > 6 || mark < 1){
                $(event.currentTarget).val("");
                $(event.currentTarget).parent().siblings('[name="updateMark"]').attr("disabled",true);
            }else if(mark == undefined){
                $(event.currentTarget).parent().siblings('[name="updateMark"]').attr("disabled",true);
            }else{ //mark is ok, check if date is also ok and if true enable button
                var date = $(event.currentTarget).parent().siblings().find('[name="dateInput"]').val();
                var regex = /^(31|30|0[1-9]|[12][0-9]|[1-9])\.(0[1-9]|1[012]|[1-9])\.((18|19|20)\d{2}|\d{2})$/;
                if (!date.match(regex)){
                    $(event.currentTarget).parent().siblings('[name="updateMark"]').attr("disabled",true);
                }else{
                    $(event.currentTarget).parent().siblings('[name="updateMark"]').attr("disabled",false);
                }
            }
        }else{ //user typed something into date input field
            var date = $(event.currentTarget).val();
            var regex = /^(31|30|0[1-9]|[12][0-9]|[1-9])\.(0[1-9]|1[012]|[1-9])\.((18|19|20)\d{2}|\d{2})$/;
            if (!date.match(regex)){
                $(event.currentTarget).parents(".form-group").addClass("has-error");
                $(event.currentTarget).siblings(".help-block").removeClass("hidden");
                $(event.currentTarget).parent().siblings('[name="updateMark"]').attr("disabled",true);
            }else{ // date is ok, check if mark is ok too and if true enable button
                $(event.currentTarget).parents(".form-group").removeClass("has-error");
                $(event.currentTarget).siblings(".help-block").addClass("hidden");
                var mark = $(event.currentTarget).parent().siblings().find('[name="markInput"]').val();
                if(mark !== ""){
                    $(event.currentTarget).parent().siblings('[name="updateMark"]').attr("disabled",false);
                }else{
                    $(event.currentTarget).parent().siblings('[name="updateMark"]').attr("disabled",true);
                }
            }
        }
    },

    deleteMark(event){
        var btnDelete = $(event.currentTarget);
        var markId = btnDelete.parents(".mark_edit").data("markid");
        if (String(markId).charAt(0) == "#"){
            delete this.__diff[markId];
        }else{
            this.__diff[markId] = {
                type: "DELETE",
                mark: btnDelete.siblings().find('[name="markInput"]').val(),
                date: btnDelete.siblings().find('[name="dateInput"]').val(),
                comment: btnDelete.siblings().find('[name="commentInput"]').val(),
                subjectId: $("#showUserMaterialTable").data("subjectid"),
                studentId: btnDelete.parents(".student").data("studentid")
            };
        }

        var mark = btnDelete.parents(".popover").siblings('[data-markid="'+markId+'"]');
        mark.popover('hide');
        mark.remove();
        console.log(this.__diff);
        this.updateAverage();
    },

    updateMark(event){
        var btnUpdate = $(event.currentTarget);
        var markId = btnUpdate.parents(".mark_edit").data("markid");
        if (markId === "#"){ // add new mark
            this.newMark(event);
        }else{ //update existing mark
            var mark = btnUpdate.siblings().find('[name="markInput"]').val();
            var date = btnUpdate.siblings().find('[name="dateInput"]').val();
            var comment = btnUpdate.siblings().find('[name="commentInput"]').val()
            if (String(markId).charAt(0) == "#"){
                this.__diff[markId] = {
                    type: "ADD",
                    mark: mark,
                    date: date,
                    comment: comment,
                    subjectId: $("#showUserMaterialTable").data("subjectid"),
                    studentId: btnUpdate.parents(".student").data("studentid")
                };
            }else{
                this.__diff[markId] = {
                    type: "UPDATE",
                    mark: mark,
                    date: date,
                    comment: comment,
                    subjectId: $("#showUserMaterialTable").data("subjectid"),
                    studentId: btnUpdate.parents(".student").data("studentid")
                };
            }

            btnUpdate.parents(".popover").siblings('[data-markid="'+markId+'"]').popover("hide");
            btnUpdate.parents(".popover").siblings('[data-markid="'+markId+'"]').attr("data-mark",mark);
            btnUpdate.parents(".popover").siblings('[data-markid="'+markId+'"]').children().first().replaceWith("<span>"+mark+"</span>");
            btnUpdate.parents(".popover").siblings('[data-markid="'+markId+'"]').find('[name="markInput"]').attr("value",mark);
            btnUpdate.parents(".popover").siblings('[data-markid="'+markId+'"]').find('[name="dateInput"]').attr("value",date);
            btnUpdate.parents(".popover").siblings('[data-markid="'+markId+'"]').find('[name="commentInput"]').text(comment);
        }

        console.log(this.__diff);
        this.updateAverage();
    },

    toggleMarkPopover(event){
        if ($("#showUserMaterialTable").data("edit") == 1){
            var mark = $(event.currentTarget);
            if (mark.siblings(".popover").find(".mark_edit").data("markid") == "#"){
                $(".new_marks").popover("hide");
            }
            if (!mark.siblings(".popover").length){
                mark.popover('show');
                mark.siblings(".popover").find('[name="markInput"]').select();
            }else if (mark.siblings(".popover").find(".mark_edit").data("markid") == mark.data("markid")){
                mark.popover("hide");
            }else{
                var markid = mark.siblings(".popover").find(".mark_edit").data("markid");
                mark.siblings('.marks[data-markid="'+markid+'"]').popover('hide');
                mark.popover('show');
                mark.siblings(".popover").find('[name="markInput"]').select();
            }
        }
    },

    toggleNewMarkPopover(event){
        var newMark =  $(event.currentTarget);
        if (newMark.siblings(".popover").find(".mark_edit").data("markid") == "#"){
            $(".new_marks").popover("hide");
        }else {
            if (newMark.siblings(".popover").length){
                $(".marks").popover("hide");
            }
            newMark.popover('show');
            newMark.siblings(".popover").find('[name="markInput"]').select();
        }
    },


    newMark(event){
        var btn = $(event.currentTarget);
        var mark = btn.siblings().find('[name="markInput"]').val();
        var date = btn.siblings().find('[name="dateInput"]').val();
        var comment = btn.siblings().find('[name="commentInput"]').val();
        var isBigMark = btn.parents(".big_marks").length ? true : false;

        var markCell = $(".marks_clone");
        var id = $(".marks_clone").find(".mark_edit").attr("data-markid");
        var clone = markCell.clone();
        clone.attr("data-markid","#"+id);
        clone.attr("data-mark",mark);
        clone.find(".mark_edit").attr("data-markid","#"+id);
        clone.find('[name="markInput"]').attr("value",mark);
        clone.find('[name="dateInput"]').attr("value",date);
        clone.find('[name="commentInput"]').text(comment);
        clone.attr("title",comment + " - " + date);
        clone.find(".form_edit_mark").before("<span>"+mark+"</span>");
        clone.removeClass("hidden");
        clone.removeClass("marks_clone");
        clone.addClass("marks");

        btn.parents(".popover").siblings(".new_marks").popover('hide');
        btn.parents(".popover").siblings(".new_marks").before(clone);
        $(".marks_clone").find(".mark_edit").attr("data-markid", parseInt(id)+1);

        $(".marks").popover({
            html: true,
            content: function () {
                return $(this).children(".form_edit_mark").html();
            },
            trigger: "manual",
            placement: "bottom"
        });

        this.__diff["#"+id] = {
            type: "ADD",
            mark: mark,
            date: date,
            comment: comment,
            subjectId: $("#showUserMaterialTable").data("subjectid"),
            studentId: btn.parents(".student").data("studentid"),
            isBigMark: isBigMark ? "1" : 0
        };
    },

    startEditing(event){
        $(".new_marks").removeClass("hidden");
        $("#showUserMaterialTable").data("edit",1);
        $(".marks").addClass("mark_hover");
        $("#editMarks").hide();
        $("#submitMarks").removeClass("hidden");
        $("#cancelEdit").removeClass("hidden");
    },

    cancelEdit(event){
        this.onActive(this.__classId,this.__subjectId);
    },

    submitMarks(event){
        var data = {
            "marks": this.__diff
        };
        this._getService('RestService').updateResource('classes/'+this.__classId+'/subjects/'+this.__subjectId+'/marks', data, (response)=>{
            if(!response.hasOwnProperty('error')){
                this.onActive(this.__classId,this.__subjectId);
            }else{
                if (response.code == "9"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dataInvalid'));
                }else if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }
        });
    }
});