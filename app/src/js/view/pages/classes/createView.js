var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'classesView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
        'click #btnCreateClass' : 'createClass'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/classes/createClass.hbs'),

    _onInitialize() {

    },
    onActive: function(){
        var model = new Coco.Model();
        this._getService('RestService').getResource('users', '', (response)=>{
            var users = response.users;
            var teachers = [];
            users.forEach(function(user, index, users){
                if (user.type == 1){
                    teachers.push(user);
                }
            });
            console.log(teachers);
            model.set("teachers",teachers);
            this.setModel(model);
            this.render();
        });
    },

    createClass(){
        var className = $('[name=displayName]').val();

        if (className.length < 2){
            $.mSnackbar(Coco.Translator.get('global.class.validation.class_too_short'));
            return;
        }

        var teacherId = $('[name=classTeacher]').val();
        var data = {
            'displayName': className
        }

        if (teacherId != -1){
            data['classTeacherId']= teacherId;
        }

        this._getService('RestService').createResource("classes/create", data, (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.messages.createClass'));
                this._getRouter().callRoute("classEdit", {id: response.resourceId});
            }else{
                if (response.code == "9"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dataInvalid'));
                }else if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }
        });
    }
});