var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'contactsView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/dashboard/index.hbs'),

    _onInitialize() {

    },
    onActive: function(){
        this._getService('RestService').getResource('dashboard/index', '', (response)=>{
            this.render(new Coco.Model(response));
        });

    }
});