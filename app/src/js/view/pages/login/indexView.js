var Coco = require('3m5-coco'),
    LayoutView = require('../../global/LayoutView.js'),
    LoginEvent = require('../../../event/loginEvent.js');

module.exports = dejavu.Class.declare({
    $name: 'LoginView',
    $extends: Coco.View,
    _anchor: '#insert',
    $inject: ['RestService'],
    toogleforget: 0,

    _events: {
        'click .forget_button': 'toogleForget',
        'submit #login_send': 'loginSend',
    },

    _autoRender: true,

    _template: require('../../../../hbs/pages/login/index.hbs'),


    _onInitialize() {

    },

    _onFirstRender() {

    },
    toogleForget(){
        if(this.toogleforget==0){
            this.$el.find('#login_form').addClass('animated zoomOut');
            setTimeout(()=>{
                this.$el.find('#login_form').hide();
                this.$el.find('#login_form').removeClass('animated zoomOut');
                this.$el.find('#forget_pw').addClass('animated zoomIn');
                this.$el.find('#forget_pw').show();
            });
            this.toogleforget = 1;
        }else{
            this.$el.find('#forget_pw').removeClass('animated zoomIn');
            this.$el.find('#forget_pw').addClass('animated zoomOut');
            setTimeout(()=>{
                this.$el.find('#forget_pw').hide();
                this.$el.find('#forget_pw').removeClass('animated zoomOut');
                this.$el.find('#login_form').addClass('animated zoomIn');
                this.$el.find('#login_form').show();
            }, 100);
            this.toogleforget = 0;
        }
    },
    loginSend(){
        var data = {};
        this.$el.find('#login_send').find('input').each(function(){
            data[$(this).attr('name')] = $(this).val();
        });
        $('#login_form').removeClass('animated wobble');
        this._getService('RestService').Login(data, (response)=>{
            console.log(this.$name + ".RESTService response data: ", response);
            console.log('response', response);
            if(typeof response.token!='undefined'){
                $.mSnackbar('Du wirst angemeldet');
                $.jStorage.set('token', response.token);
                $.jStorage.set('userid', response.userid);
                this._dispatchEvent(new LoginEvent(response, 'login'));
            }else{
                $('#login_form').addClass('animated wobble');
                $.mSnackbar('Logindaten sind nicht korrekt');
            }

        });
        return false;
    }

})