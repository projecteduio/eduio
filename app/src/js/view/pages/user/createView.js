var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'createView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    i:0,
    _events: {
        'change [name=togglePassword]': 'togglePassword',
        'click .editUserUserGroup': 'editUserUserGroup',
        'click #btnCreateUser': 'createUser'

        //'EVENT CSS-Selector': 'eventhandler-function'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/user/createUser.hbs'),

    _onInitialize() {
    },
    onActive: function(){
        var model = new Coco.Model();
        this.setModel(model);
        model.set("randomPassword", this.generatePassword());
        this._getService('RestService').getResource('classes', '', (response)=>{
            model.set("classes",response.classes);
            if(model.has("subjects")){
                this.render();
            }
        });
        this._getService('RestService').getResource('subjects', '', (response)=>{
            model.set("subjects",response.subjects);
            if(model.has("classes")){
                this.render();
            }
        });
    },

    newCustomPassword() {
        this.$el.find('#passwordRow').removeClass("hidden");
        this.$el.find('#generatedPassword').addClass("hidden");
        this.$el.find('#passwordRow input .ng-invalid').focus();
    },

    generatePassword() {
        var Password = {
            _pattern: /[a-zA-Z0-9_\-\+\.]/,


            _getRandomByte: function () {
                // http://caniuse.com/#feat=getrandomvalues
                if (window.crypto && window.crypto.getRandomValues) {
                    var result = new Uint8Array(1);
                    window.crypto.getRandomValues(result);
                    return result[0];
                }
                else if (window.msCrypto && window.msCrypto.getRandomValues) {
                    var result = new Uint8Array(1);
                    window.msCrypto.getRandomValues(result);
                    return result[0];
                }
                else {
                    return Math.floor(Math.random() * 256);
                }
            },

            generate: function (length) {
                return Array.apply(null, {'length': length})
                    .map(function () {
                        var result;
                        while (true) {
                            result = String.fromCharCode(this._getRandomByte());
                            if (this._pattern.test(result)) {
                                return result;
                            }
                        }
                    }, this)
                    .join('');
            },
        };
        return Password.generate(10);
    },

    togglePassword(event) {
        if ($(event.currentTarget).is('#generatePassword')){
            this.$el.find('#generatedPassword').removeClass("hidden");
            this.$el.find('#passwordRow').addClass("hidden");
            this.$el.find('#generatedPassword input').val(this.generatePassword());
            this.$el.find('#generatedPassword input').select();
            this.$el.find('#generatedPassword input').focus();
        }else{
            this.$el.find('#passwordRow').removeClass("hidden");
            this.$el.find('#generatedPassword').addClass("hidden");
            this.$el.find('#passwordRow input .ng-invalid').focus();
        }
    },

    editUserUserGroup (event) {
        this.$el.find(".editUserUserGroup").removeClass("active");
        $(event.currentTarget).addClass("active");
        $(event.currentTarget).find("[name=group]").prop("checked", true);
        switch ($(event.currentTarget).data('uuid')){
            case 0:
                this.$el.find("#editStudentClass").addClass("hidden");
                break;
            case 1:
                this.$el.find("#editStudentClass").addClass("hidden");
                break;
            case 2:
                this.$el.find("#editStudentClass").removeClass("hidden");
                break;
            case 3:
                this.$el.find("#editStudentClass").addClass("hidden");
                break;
        }
    },

    addPermission() {
        let $row   = this.$el.find(".permission_clone");
        let id = $row.attr("data-id");

        let $clone = $row.clone();
        $clone.removeClass("hidden");
        $clone.removeClass("permission_clone");
        $clone.addClass("permission");
        $clone.find('[name=access]').attr("name","accessN" + id);
        $row.after($clone);
        $row.attr("data-id", parseInt(id)+1);
    },

    deletePermission(event) {
        $(event.currentTarget).parent().parent().remove();
    },

    createUser(){
        let userData = {
            'active': $('[name=active]').is(':checked') ? '1' : '0'
        };

        let firstname = $('[name=firstname]').val();
        let lastname = $('[name=lastname]').val();

        if (firstname.length < 2){
            $.mSnackbar(Coco.Translator.get('global.user.show.validation.firstname.invalid'));
            return;
        }else{
            userData.firstname = firstname;
        }

        if (lastname.length < 2){
            $.mSnackbar(Coco.Translator.get('global.user.show.validation.lastname.invalid'));
            return;
        }else{
            userData.lastname = lastname;
        }

        userData.type = $('[name=group]:checked').val();
        if (userData.type == '2'){
            userData.class = $('[name=class]').val();
        }
        let gender = $('[name=gender]').val();
        if (gender == ""){
            $.mSnackbar(Coco.Translator.get('global.user.show.validation.gender.choose'));
            return;
        }else{
            userData.gender = gender;
        }
        let generatePassword = $('[name=togglePassword]:checked').val();
        if (generatePassword == 'newAuto'){
            let p1 = $('[name=generatedPasswordOut]').val();
            if (p1.length < 8){
                $.mSnackbar(Coco.Translator.get('global.user.show.validation.password.too_short'));
                return;
            }
            userData.password = p1;
        }else if (generatePassword == 'newCustom'){
            let p1 = $('[name=password]').val();
            let p2 = $('[name=confirm]').val();
            if (p1 != p2){
                $.mSnackbar(Coco.Translator.get('global.user.show.validation.password.not_equal'));
                return;
            }
            if (p1.length < 8){
                $.mSnackbar(Coco.Translator.get('global.user.show.validation.password.too_short'));
                return;
            }
            userData.password = p1;
        }

        this._getService('RestService').createResource("users/create", userData, (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.messages.createUser'));
                this._getRouter().callRoute("userEdit", {id: response.resourceId});
            }else{
                if (response.code == "9"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dataInvalid'));
                }else if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }

        });
    }

});

