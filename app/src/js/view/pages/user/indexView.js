var Coco = require('3m5-coco');
$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

module.exports = dejavu.Class.declare({
    $name: 'userView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    i:0,
    _events: {
        'click button[data-id="searchOpen"]': 'openSearch',
        'click button[data-id="searchClose"]' : 'closeSearch',
        'click button[data-id="btnDeleteSelected"]' : 'showDeleteSelectedModal',
        'click input[type="checkbox"]' : 'toggletr',
        'click input[type="checkbox"].all' : 'toggleall',
        'click td.link' : 'editUser',
        'click button[data-id="btnDeleteUser"]' : 'deleteEntry',
        'click #deleteUserModalCancel' : 'cancelDelete',
        'click #deleteUserModalAction' : 'deleteUser',
        'click #deleteUsersModalCancel' : 'cancelDeleteSelected',
        'click #deleteUsersModalAction' : 'deleteUsers',
        'keyup [data-id="materialTableSearch"]' : 'searchUser'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/user/showUsers.hbs'),

    _onInitialize() {
    },
    onActive: function(){
        this._getService('RestService').getResource('users', '', (response)=>{
            this.setModel(new Coco.Model(response));
            this.render();
        });
    },

    openSearch() {
        this.$el.find('.materialTitle').hide();
        this.$el.find('.materialSearch').show();
        this.$el.find('.materialSearch input').val('');
        this.$el.find('.materialSearch input').focus();
    },

    closeSearch(){
        this.$el.find('.materialSearch').hide();
        this.$el.find('.materialTitle').show();
    },
    searchUser(event){
        if(!$('.errorFound').hasClass('hide')){
            $('.errorFound').addClass('hide');
        }
        if($(event.currentTarget).val()==""){
            $('tr.link').removeClass('hide');
        }else{
            $('tr.link').addClass('hide');
            $('tr.link:contains('+$(event.currentTarget).val()+')').removeClass('hide');
            if($('tr.link').not('.hide').length==0){
                $('.errorFound').removeClass('hide');
            }
        }

    },
    openCheckboxCounter(){
        this.$el.find('.materialTitle').hide();
        this.$el.find('.materialSelect').show();
    },

    toggletr(event){
        if($(event.currentTarget).parents('tr').hasClass('active')){
            $(event.currentTarget).parents('tr').removeClass('active');
        }
        else{
            $(event.currentTarget).parents('tr').addClass('active');
        }
        this.updateSelect();
    },

    toggleall(event){
        if($(event.currentTarget).attr('data-active') == 1) {
            $(event.currentTarget).attr('data-active', 0);
            this.$el.find('tbody tr.active input[type="checkbox"]').trigger('click');
            this.$el.find('tbody input[type="checkbox"]').trigger('click');
            this.$el.find('tbody tr.active input[type="checkbox"]').trigger('click');
        }
        else{
            $(event.currentTarget).attr("data-active", 1);
            this.$el.find('tbody tr.active input[type="checkbox"]').trigger('click');
            this.$el.find('tbody input[type="checkbox"]').trigger('click');
        }

    },

    updateSelect(){
        this.countSelected = this.$el.find('tbody tr.active').length;
        if(this.countSelected > 0){
            this.$el.find('.materialTitle').hide();
            this.$el.find('.materialSelect').show();
            this.$el.find('.materialSelect span').text(this.countSelected);
        }
        else{
            this.$el.find('.materialSelect').hide();
            this.$el.find('.materialTitle').show();
        }


    },
    editUser(event){
        var uuid = $(event.currentTarget).parent().attr("data-uuid");
        //this._getService('RouterService').callRoute('marks');
        //this._getService("router").callRoute("userEdit", {id: uuid});
        this._getRouter().callRoute("userEdit", {id: uuid});
    },

    deleteEntry(event){
        var uuid = $(event.currentTarget).parents('tr.link').attr('data-uuid');
        $("#deleteUserModal").attr('data-uuid',uuid);
        $("#deleteUserModal").show();
    },

    cancelDelete(){
        $("#deleteUserModal").hide();
    },

    cancelDeleteSelected(){
        $("#deleteUsersModal").hide();
        $(".selector").each(function(){
            var checkbox = this;
            $(this).prop("checked", false);
            $(this).parent().parent().parent().removeClass("active");
        });
        this.updateSelect();
    },

    deleteUser(){
        var uuid = $("#deleteUserModal").attr('data-uuid');
        this._getService('RestService').deleteResource("users/"+uuid, '', (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.messages.deleteUser'));
                this._getService('RestService').getResource('users', '', (response)=>{
                    this.setModel(new Coco.Model(response));
                    this.render();
                });
            }else{
                if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }

        });
        return false;
    },

    showDeleteSelectedModal(){
        $("#deleteUsersModal").show();
    },

    deleteUsers(){
        var userIds = new Array();
        $(".selector:checked").each(function(){
            userIds.push($(this).parent().parent().parent().attr('data-uuid'));
        });
        let data = {
            "users": userIds
        };
        this._getService('RestService').deleteResource("users", data, (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.messages.deleteUsers'));
                this._getService('RestService').getResource('users', '', (response)=>{
                    this.setModel(new Coco.Model(response));
                    this.render();
                });
            }else{
                if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }

        });
        return false;
    }
});