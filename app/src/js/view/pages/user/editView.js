var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'editView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    i:0,
    _events: {
        'click #newCustomPassword': 'newCustomPassword',
        'click #keepPassword': 'keepPassword',
        'click #generatePassword': 'generatePassword',
        'change #active': 'toggleDeleteCard',
        'click .editUserUserGroup': 'editUserUserGroup',
        'click #btnAddPermission': 'addPermission',
        'click .btnDeletePermission': 'deletePermission',
        'click #btnSubmitPermissions': 'submitPermissions',
        'click #updateUser': 'updateUser',
        'click #deleteUser': 'deleteUser'

        //'EVENT CSS-Selector': 'eventhandler-function'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/user/editUser.hbs'),

    _onInitialize() {
    },
    onActive: function(id){
        var model = new Coco.Model();
        this.setModel(model);

        this._getService('RestService').getResource('users/'+id, '', (response)=>{
            model.set("user",response.user);
            if (model.has("classes") && model.has("subjects") && model.has("permissions")){
                this.render();
            }
        });
        this._getService('RestService').getResource('classes', '', (response)=>{
            model.set("classes",response.classes);
            if (model.has("user") && model.has("subjects") && model.has("permissions")){
                this.render();
            }
        });
        this._getService('RestService').getResource('users/'+id+"/permissions", '', (response)=>{
            model.set("permissions",response.permissions);
            if (model.has("classes") && model.has("subjects") && model.has("user")){
                this.render();
            }
        });
        this._getService('RestService').getResource('subjects', '', (response)=>{
            model.set("subjects",response.subjects);
            if (model.has("classes") && model.has("user") && model.has("permissions")){
                this.render();
            }
        });
    },

    newCustomPassword() {
        this.$el.find('#passwordRow').removeClass("hidden");
        this.$el.find('#generatedPassword').addClass("hidden");
        this.$el.find('#passwordRow input .ng-invalid').focus();
    },

    generatePassword() {
        var Password = {
            _pattern : /[a-zA-Z0-9_\-\+\.]/,

            _getRandomByte : function()
            {
                // http://caniuse.com/#feat=getrandomvalues
                if(window.crypto && window.crypto.getRandomValues)
                {
                    var result = new Uint8Array(1);
                    window.crypto.getRandomValues(result);
                    return result[0];
                }
                else if(window.msCrypto && window.msCrypto.getRandomValues)
                {
                    var result = new Uint8Array(1);
                    window.msCrypto.getRandomValues(result);
                    return result[0];
                }
                else
                {
                    return Math.floor(Math.random() * 256);
                }
            },

            generate : function(length)
            {
                return Array.apply(null, {'length': length})
                    .map(function()
                    {
                        var result;
                        while(true)
                        {
                            result = String.fromCharCode(this._getRandomByte());
                            if(this._pattern.test(result))
                            {
                                return result;
                            }
                        }
                    }, this)
                    .join('');
            }

        };
        this.$el.find('#generatedPassword').removeClass("hidden");
        this.$el.find('#passwordRow').addClass("hidden");
        this.$el.find('#generatedPassword input').val(Password.generate(10));
        this.$el.find('#passwordRow input').val(this.$el.find('#generatedPassword input').getValue());
        this.$el.find('#generatedPassword input').select();
        this.$el.find('#generatedPassword input').focus();
    },

    keepPassword() {
        this.$el.find('#passwordRow').addClass("hidden");
        this.$el.find('#generatedPassword').addClass("hidden");
    },

    toggleDeleteCard (event) {
        if (!$(event.target).is(':checked')){
            this.$el.find('#deleteUser').removeClass("hidden");
        }else{
            this.$el.find('#deleteUser').addClass("hidden");
        }
    },

    editUserUserGroup (event) {
        this.$el.find(".editUserUserGroup").removeClass("active");
        $(event.currentTarget).addClass("active");
        $(event.currentTarget).find("[name=group]").prop("checked", true);


        switch ($(event.currentTarget).data('uuid')){
            case 0:
                this.$el.find("#editTeacherPermissionsTable").addClass("hidden");
                this.$el.find("#editStudentClass").addClass("hidden");
                break;
            case 1:
                this.$el.find("#editTeacherPermissionsTable").removeClass("hidden");
                this.$el.find("#editStudentClass").addClass("hidden");
                break;
            case 2:
                this.$el.find("#editTeacherPermissionsTable").addClass("hidden");
                this.$el.find("#editStudentClass").removeClass("hidden");
                break;
            case 3:
                this.$el.find("#editTeacherPermissionsTable").removeClass("hidden");
                this.$el.find("#editStudentClass").addClass("hidden");
                break;
        }
    },

    addPermission() {
        let $row   = this.$el.find(".permission_clone");
        let id = $row.attr("data-id");

        let $clone = $row.clone();
        $clone.removeClass("hidden");
        $clone.removeClass("permission_clone");
        $clone.addClass("permission");
        $clone.find('[name=access]').attr("name","accessN" + id);
        $row.after($clone);
        $row.attr("data-id", parseInt(id)+1);
    },

    deletePermission(event) {
        $(event.currentTarget).parent().parent().remove();
    },

    submitPermissions(event){
        let permissions = [];
        let i=0;
        let permissionKeys = [];
        $(".permission").each(function() {
            let permission = $(this);
            let access = permission.find("[name=" + permission.find(".access").attr("name") + "]:checked").val();
            let subjectId = permission.find("[name=subject]").val();
            let classId = permission.find("[name=class]").val();
            if (subjectId != "" && classId != "") {
                let permissionKey = classId + "|" + subjectId;
                if (!permissionKeys.includes(permissionKey)){
                    permissions[i] = {
                        subjectId: subjectId,
                        classId: classId,
                        access: access
                    };
                    i++;
                    permissionKeys.push(permissionKey);
                }
            }
        });

        let data = {
            permissions: permissions
        };
        let id = this.getModel().get("user").uuid;
        this._getService('RestService').updateResource("users/"+id+"/permissions", data, (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.messages.updateUser'));
                this._getService('RestService').getResource('users/'+id, '', (response)=>{
                    this.setModel(new Coco.Model(response));
                });
            }else{
                if (response.code == "9"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dataInvalid'));
                }else if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }

        });
    },

    updateUser(){
        let id = this.getModel().get("user").uuid;
        let userData = {
            'firstname': $('[name=firstname]').val(),
            'lastname': $('[name=lastname]').val(),
            'active': $('[name=active]').is(':checked') ? '1' : '0'
        };

        userData.type = $('[name=group]:checked').val();
        if (userData.type == '2'){
            userData.class = $('[name=class]').val();
        }
        let gender = $('[name=gender]').val();
        if (gender == ""){
            $.mSnackbar(Coco.Translator.get('global.user.show.validation.gender.choose'));
            return;
        }else{
            userData.gender = gender;
        }
        let generatePassword = $('[name=generatePassword]:checked').val();
        if (generatePassword == 'newAuto'){
            let p1 = $('[name=generatedPasswordOut]').val();
            if (p1.length < 8){
                $.mSnackbar(Coco.Translator.get('global.user.show.validation.password.too_short'));
                return;
            }
            userData.password = p1;
        }else if (generatePassword == 'newCustom'){
            let p1 = $('[name=password]').val();
            let p2 = $('[name=confirm]').val();
            if (p1 != p2){
                $.mSnackbar(Coco.Translator.get('global.user.show.validation.password.not_equal'));
                return;
            }
            if (p1.length < 8){
                $.mSnackbar(Coco.Translator.get('global.user.show.validation.password.too_short'));
                return;
            }
            userData.password = p1;
        }

        this._getService('RestService').updateResource("users/"+id, userData, (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.messages.updateUser'));
                this._getService('RestService').getResource('users/'+id, '', (response)=>{
                    this.setModel(new Coco.Model(response));
                    this.render();
                });
            }else{
                if (response.code == "9"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dataInvalid'));
                }else if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }

        });
    },

    deleteUser(){
        let id = this.getModel().get("user").uuid;
        this._getService('RestService').deleteResource("users/"+id, '', (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.messages.deleteUser'));
                this._getRouter().callRoute("user");
            }else{
                if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }
        });
        return false;
    }

});

