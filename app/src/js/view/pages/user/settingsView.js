var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'settingsView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
        'input [name=newPassword]': 'checkPasswordLength',
        'input [name=newPasswordConfirmation]': 'confirmPasswords',
        'click #btnUpdatePassword': 'updatePassword',
        'change #update_year_chooser': 'updateYearChanged',
        'click #btnUpdateCurrentYear': 'updateCurrentYear',
        'click #btnUpdateYear': 'updateYear',
        'click #btnCreateYear': 'createYear',
        'click #btnDeleteYear': 'deleteYear',
        'click #btnShowDeleteYearModal': 'showDeleteYearModal',
        'click #btnCancelDeleteYear': 'cancelDeleteYear'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/user/settings.hbs'),

    _onInitialize() {

    },
    onActive: function(){
        var progress = 0;
        var ready = 2;
        var model = new Coco.Model();
        model.set("type",$.jStorage.get("usertype"));
        this._getService('RestService').getResource("settings/years", '', (response)=>{
            model.set("years",response.years);
            progress++;
            if (progress == ready){
                this.render();
            }
        });
        this._getService('RestService').getResource("settings/years/current", '', (response)=>{
            model.set("current",response.year);
            progress++;
            if (progress == ready){
                this.render();
            }
        });
        this.setModel(model);
        this.render();
    },

    checkPasswordLength(event){
        this.confirmPasswords();
        var passwordBox = $(event.currentTarget)
        var password = passwordBox.val();
        if (password.length < 6 ){
            passwordBox.parent().addClass("has-error");
            passwordBox.parent().removeClass("has-success");
            passwordBox.siblings(".glyphicon-remove").removeClass("hidden");
            passwordBox.siblings(".glyphicon-ok").addClass("hidden");
        }else{
            passwordBox.parent().addClass("has-success");
            passwordBox.parent().removeClass("has-error");
            passwordBox.siblings(".glyphicon-remove").addClass("hidden");
            passwordBox.siblings(".glyphicon-ok").removeClass("hidden");
        }
    },

    confirmPasswords(){
        var passwordBox = $("#newPassword");
        var confirmBox = $("#newPasswordConfirmation");
        if (passwordBox.val().length != 0 && confirmBox.val().length != 0 && passwordBox.val() == confirmBox.val()){
            confirmBox.parent().addClass("has-success");
            confirmBox.siblings(".glyphicon-ok").removeClass("hidden");
        }else{
            confirmBox.parent().removeClass("has-success");
            confirmBox.siblings(".glyphicon-ok").addClass("hidden");
        }
    },

    updatePassword(){
        var data = {
            "newpassword": $("#newPassword").val(),
            "oldpassword": $("#oldPassword").val()
        };
        if (data['newpassword'] != $("#newPasswordConfirmation").val()){
            $.mSnackbar(Coco.Translator.get('global.settings.password.validation.confirmation_failed'));
            return;
        }
        if (data['newpassword'].length < 6){
            $.mSnackbar(Coco.Translator.get('global.settings.password.validation.too_short'));
            return;
        }
        var userid = $.jStorage.get('userid');
        this._getService('RestService').updateResource("users/"+userid+"/password", data, (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.messages.updatePassword'));
                this.onActive();
            }else{
                if (response.code == "9"){
                    $.mSnackbar(Coco.Translator.get('global.settings.password.validation.new_invalid'));
                }else if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else if (response.code == "5"){
                    $.mSnackbar(Coco.Translator.get('global.settings.password.validation.old_invalid'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }
        });
    },

    updateYearChanged(event){
        if ($(event.currentTarget).val() != -1){
            $("#update_year").val($(event.currentTarget).children("option:selected").text());
            $("#update_year").select();
        }else{
            $("#update_year").val("");
        }
    },


    updateCurrentYear(event){
        var yearId = $("#current_year").val();
        this._getService('RestService').updateResource("settings/years/current", {"year": yearId}, (response)=>{
            if(!response.hasOwnProperty('error')){
                $.mSnackbar(Coco.Translator.get('global.settings.year.current_updated'));
                this.onActive();
            }else{
                if (response.code == "7"){
                    $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                }else if (response.code == "1"){
                    $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                }else{
                    $.mSnackbar(Coco.Translator.get('global.messages.error'));
                }
            }
        });
    },

    updateYear(event){
        var yearId = $("#update_year_chooser").val();
        if (yearId != -1) {
            var newTitle = $("#update_year").val();
            if (newTitle != ""){
                this._getService('RestService').updateResource("settings/years/"+yearId, {"displayName": newTitle}, (response)=>{
                    if(!response.hasOwnProperty('error')){
                        $.mSnackbar(Coco.Translator.get('global.settings.year.year_updated'));
                        this.onActive();
                    }else{
                        if (response.code == "7"){
                            $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                        }else if (response.code == "1"){
                            $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                        }else{
                            $.mSnackbar(Coco.Translator.get('global.messages.error'));
                        }
                    }
                });
            }
        }
    },

    showDeleteYearModal(event){
        var yearId = $("#delete_year_chooser").val();
        if (yearId != -1) {
            $("#deleteYearModal").show();
        }
    },

    cancelDeleteYear(event){
        $("#deleteYearModal").hide();
    },

    deleteYear(event){
        var yearId = $("#delete_year_chooser").val();
        if (yearId != -1) {
            this._getService('RestService').deleteResource("settings/years/"+yearId, '', (response)=>{
                if(!response.hasOwnProperty('error')){
                    $.mSnackbar(Coco.Translator.get('global.settings.year.year_deleted'));
                    this.onActive();
                }else{
                    if (response.code == "7"){
                        $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                    }else if (response.code == "1"){
                        $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                    }else{
                        $.mSnackbar(Coco.Translator.get('global.messages.error'));
                    }
                }
            });
        }
    },

    createYear(event){
        var newTitle = $("#create_year").val();
        if (newTitle != ""){
            this._getService('RestService').createResource("settings/years/create", {"displayName": newTitle}, (response)=>{
                if(!response.hasOwnProperty('error')){
                    $.mSnackbar(Coco.Translator.get('global.settings.year.year_created'));
                    this.onActive();
                }else{
                    if (response.code == "7"){
                        $.mSnackbar(Coco.Translator.get('global.messages.dbError'));
                    }else if (response.code == "1"){
                        $.mSnackbar(Coco.Translator.get('global.messages.loginExpired'));
                    }else{
                        $.mSnackbar(Coco.Translator.get('global.messages.error'));
                    }
                }
            });
        }
    }

});