var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'userMarksView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/user/marksUser.hbs'),

    _onInitialize() {

    },
    onActive: function(){
        this.setModel(new Coco.Model({
            type: 0,
            teachers:
                [
                    {"id": "1", "name": "Müller"},
                    {"id": "1", "name": "test"},
                    {"id": "1", "name": "test"},
                    {"id": "1", "name": "test"},
                    {"id": "1", "name": "test"}

                ]

        }));
        this.render();
    },
});