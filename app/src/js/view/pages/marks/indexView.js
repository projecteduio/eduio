var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'MarksView',
    $extends: Coco.View,
    $inject: ['RestService', 'router'],
    i:0,
    _events: {
        'click .edit_marks_disabled' : 'CalcMode',
        'click .edit_marks_enabled' : 'closeCalcMode',
        'input input.new_marks' : 'newMarks'


        //'EVENT CSS-Selector': 'eventhandler-function'
    },
    _autoRender: false,
    _template: require('../../../../hbs/pages/marks/showMarks.hbs'),

    _onInitialize() {
    },
    onActive: function(){

        var userid = $.jStorage.get('userid');

        this._getService('RestService').getResource('users/'+userid+'/marks', '', (response)=>{
            this.setModel(new Coco.Model(response));
            this.render();
        });
    },

    render(){
        this.$super();
        this.onRender();
    },

    onRender(){
        $(".new_marks").hide();
        this.updateAverage();
        $("body").tooltip({
            selector: ".marks, .md-btn"
        });
    },

    updateAverage(){
        var finalMarks= [];
        var subjectsCount = 0;
        $(".subject").each(function(){
            subjectsCount++;
            var subject = $(this);
            var bigMarks = [];
            var smallMarks = [];
            var averageSmall = 0;
            var averageBig = 0;

            //retrieve marks
            subject.find(".big_marks .marks").each(function(){
                bigMarks.push(parseInt($(this).text()));
            });
            subject.find(".big_marks .new_marks").each(function(){
                if ($(this).val() != "" && $(this).is(":visible"))
                    bigMarks.push(parseInt($(this).val()));
            });
            subject.find(".small_marks .marks").each(function(){
                smallMarks.push(parseInt($(this).text()));
            });
            subject.find(".small_marks .new_marks").each(function(){
                if ($(this).val() != "" && $(this).is(":visible"))
                    smallMarks.push(parseInt($(this).val()));
            });

            console.log(smallMarks);
            console.log(bigMarks);

            // calculate average
            if (smallMarks.length > 0) {
                smallMarks.forEach(function (mark) {
                    averageSmall += mark;
                });
                averageSmall /= smallMarks.length;
            }

            if (bigMarks.length > 0) {
                bigMarks.forEach(function (mark) {
                    averageBig += mark;
                });
                averageBig /= bigMarks.length;
            }

            averageBig = Math.round(averageBig * 10) / 10;
            averageSmall = Math.round(averageSmall * 10) / 10;

            // calculate final mark
            var finalMark = Math.round(((Math.round(averageBig)+Math.round(averageSmall))/2)*10) / 10;

            //print everything to screen
            if (averageSmall != 0){
                $(this).find(".average_small").text(averageSmall);
            }else{
                $(this).find(".average_small").text("");
            }
            if (averageBig != 0){
                $(this).find(".average_big").text(averageBig);
            }else{
                $(this).find(".average_big").text("");
            }
            if (averageBig != 0 && averageSmall != 0){
                $(this).find(".total_mark").text(finalMark);
                finalMarks.push(finalMark);
            }else{
                $(this).find(".total_mark").text("");
            }

            if (finalMarks.length == subjectsCount && subjectsCount > 0){
                var totalAverage = 0;
                finalMarks.forEach(function(mark){
                    totalAverage += mark;
                });
                totalAverage /= subjectsCount;
                totalAverage = Math.round(totalAverage*10)/10;
                $(".average_total_marks").text(totalAverage);
            }
        });
    },
    CalcMode(event){
       var subjectId = $(event.currentTarget).attr('data-subjectId');
        $("tr.link[data-subjectId='"+subjectId+"'] .edit_marks_disabled").parent().hide();
        $("tr.link[data-subjectId='"+subjectId+"'] input.new_marks").show();
        $("tr.link[data-subjectId='"+subjectId+"'] input.new_marks").first().focus();
        $("tr.link[data-subjectId='"+subjectId+"'] .edit_marks_enabled").parent().show();
        this.updateAverage();
    },
    closeCalcMode(event){
        var subjectId = $(event.currentTarget).attr('data-subjectId');
        $(".link[data-subjectId='" + subjectId + "'] .set_new_marks").remove();
        $("tr.link[data-subjectId='" + subjectId + "'] input.new_marks").hide();
        $("tr.link[data-subjectId='" + subjectId + "'] .edit_marks_enabled").parent().hide();
        $("tr.link[data-subjectId='" + subjectId + "'] .edit_marks_disabled").parent().show();
        this.updateAverage();
    },

    newMarks(event){
        var mark = $(event.currentTarget).val();
        console.log(mark);
        if (mark.length > 1) {
            mark = mark.charAt(0);
            $(event.currentTarget).val(mark);
            return;
        }
        if (mark.length == 0) { //mark field has been emptied
            var counter = 0;
            $(event.currentTarget).parent().children(".new_marks").each(function (index, element) {
                if ($(element).val() == "") {
                    counter++;
                    if (counter > 1) {
                        $(event.currentTarget).remove();
                    }
                }
            });
            $(event.currentTarget).parent().children(".new_marks").last().focus()
        }
        if (mark > 6 || mark < 1) { //mark is invalid -> empty field
            $(event.currentTarget).val("");
            var counter = 0;
            $(event.currentTarget).parent().children(".new_marks").each(function (index, element) {
                if ($(element).val() == "") {
                    counter++;
                    if (counter > 1) {
                        $(event.currentTarget).remove();
                    }
                }
            });
            $(event.currentTarget).parent().children(".new_marks").last().focus()
        } else { //mark is ok
            $(event.currentTarget).parent().children(".new_marks").each(function (index, element) {
                if ($(element).val() == "") {
                    $(event.currentTarget).remove();
                    return false;
                }
            });
            $(event.currentTarget).parent().children(".new_marks").last().after('<input type="number" min="1" max="6" class="new_marks new_marks_big disable_spinner">');
            $(event.currentTarget).parent().children(".new_marks").last().focus()
        }
        this.updateAverage();
    },

});