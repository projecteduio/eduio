var Coco = require('3m5-coco'),
LoginView = require('../../view/pages/login/indexView.js');
module.exports = dejavu.Class.declare({
    //className
    $name: 'LayoutView',
    //inheritance
    $extends: Coco.View,
    $inject: ['RestService'],
    //jQuery selector to add this view
    //_anchor: '.testView',
    _anchor: '#insert',
    //jQuery events to handle directly
    _events: {
        //'EVENT CSS-Selector': 'eventhandler-function'
        'click #logout': 'logout'
    },

    _autoRender: true,

    _template: require('../../../hbs/elements/layout.hbs'),
    _getHBSModel() {
        return this.$super();
    },
    logout(){
        $.jStorage.flush();
        location.reload();
    }
})