var Coco = require('3m5-coco');

module.exports = Coco.ChildView = dejavu.Class.declare({
    $name: 'tableChildView',
    $extends: Coco.ChildView,
    $inject: ['RestService'],
    _anchor: 'div[data-id="table"]',
    i:0,
    _events: {
        'click button[data-id="searchOpen"]': 'openSearch',
        'click button[data-id="searchClose"]' : 'closeSearch',
        'click input[type="checkbox"]' : 'toggletr',


        //'EVENT CSS-Selector': 'eventhandler-function'
    },
    _autoRender: true,
    _template: require('../../../../hbs/elements/table.hbs'),

    _onInitialize() {
        //this.render();
    },

    openSearch() {
        this.$el.find('.materialTitle').hide();
        this.$el.find('.materialSearch').show();
        this.$el.find('.materialSearch input').val('');
        this.$el.find('.materialSearch input').focus();
    },

    closeSearch(){
        this.$el.find('.materialSearch').hide();
        this.$el.find('.materialTitle').show();
    },

    openCheckboxCounter(){
        this.$el.find('.materialTitle').hide();
        this.$el.find('.materialSelect').show();
    },

    toggletr(event){
        if($(event.currentTarget).parents('tr').hasClass('active')){
            $(event.currentTarget).parents('tr').removeClass('active');
        }
        else{
            $(event.currentTarget).parents('tr').addClass('active');
        }
        this.updateSelect();
    },

    toggleall(){
        if($(event.currentTarget).parents('tr').hasClass('active')){
            $(event.currentTarget).parents('tr').removeClass('active');
        }
        else{
            $(event.currentTarget).parents('tr').addClass('active');
        }
        this.updateSelect();
    },
    updateSelect(){
        this.countSelected = this.$el.find('tbody tr.active').length;
        if(this.countSelected > 0){
            this.$el.find('.materialTitle').hide();
            this.$el.find('.materialSelect').show();
            this.$el.find('.materialSelect span').text(this.countSelected);
        }
        else{
            this.$el.find('.materialSelect').hide();
            this.$el.find('.materialTitle').show();
        }


    }


});