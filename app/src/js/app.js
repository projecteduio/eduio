/**
 * @author (c) Andreas Wiedenfeld <andreas.wiedenfeld@3m5.de>
 * updated at 19.06.2015
 */
/** @namespace **/

var Coco            = require('3m5-coco'),
LayoutView          = require('./view/global/layoutView.js'),
UserView            = require('./view/pages/user/indexView.js'),
UserEditView        = require('./view/pages/user/editView.js'),
UserCreateView      = require('./view/pages/user/createView.js'),
SettingsView        = require('./view/pages/user/settingsView.js'),
LoginView           = require('./view/pages/login/indexView.js'),
ClassesView         = require('./view/pages/classes/showClassesView.js'),
ClassesEditView     = require('./view/pages/classes/editClassesView.js'),
SubjectsView        = require('./view/pages/classes/subjectsView.js'),
SubjectView         = require('./view/pages/classes/subjectView.js'),
EditView            = require('./view/pages/classes/editView.js'),
StudentsView        = require('./view/pages/classes/studentsView.js'),
StudentView         = require('./view/pages/classes/studentView.js'),
ClassCreateView     = require('./view/pages/classes/createView.js'),
MarksView           = require('./view/pages/marks/indexView.js');
require('./service/RestService.js');
require('./helper/HandlebarsHelpers.js');


var EDUIO = dejavu.Class.declare({
    $extends: Coco.ServiceProvider,
    $inject: ['RestService'],
    _onServicesInjected() {
       this.initLayout();
    },
    initLayout(login){
        this._getService('RestService').getLayout((response)=>{
            Coco.Translator.loadMessages(Coco.config.s3+'/lang/de.json', () => {
                if(response.logout == 1){
                    var loginView = new LoginView();
                    loginView.addEventListener('login', (event) => {
                        this.initLayout(true);
                    });
                }else{
                    if(login){
                        $.mSnackbar(Coco.Translator.get('global.welcome'));
                    }
                    this.initRouter(response);
                }
            });
        });
    },
    initRouter(response){
        var layoutView = new LayoutView(new Coco.Model(response));
        switch(parseInt(response.type)){
            case 0:
                $(location).attr('href', '/#/user');
                break;
            case 1:
                $(location).attr('href', '/#/classes');
                break;
            case 2:
                $(location).attr('href', '/#/marks');
                break;
            case 3:
                $(location).attr('href', '/#/classes');
                break;
        }
        layoutView.addEventListener(Coco.Event.RENDER, (event) => {
            var routing = {
                login: {
                    path: '/login',
                    view: LoginView
                },
                user: {
                    path: '/user',
                    view: UserView
                },
                userCreate: {
                    path: '/user/create',
                    view: UserCreateView
                },
                userEdit: {
                    path: '/user/:id',
                    view: UserEditView
                },
                classes:{
                    path: '/classes',
                    view: ClassesView
                },
                classCreate:{
                    path: '/classes/create',
                    view: ClassCreateView
                },
                subjects: {
                    path: '/classes/:id/subjects',
                    view: SubjectsView
                },
                subject: {
                    path: '/classes/:id/subjects/:subjectId',
                    view: SubjectView
                },
                marks: {
                    path: '/marks',
                    view: MarksView
                },
                settings: {
                    path: '/settings',
                    view: SettingsView
                }
            };
            switch(parseInt(response.type)){
                case 3:
                case 0:
                    routing['classesEdit'] = {
                        path: '/classes/edit',
                        view: ClassesEditView
                    };
                    routing['classEdit'] = {
                        path: '/classes/:id',
                        view: EditView
                    };
                    break;
                case 1:
                    routing['students'] = {
                        path: '/classes/:id',
                        view: StudentsView
                    };
                    routing['student'] = {
                        path: '/classes/:id/students/:studentid',
                        view: StudentView
                    };
                    break;
                case 2:
                    break;
            }
            new Coco.Router('#put_content', routing, '/');
        });
    }
});

$(document).ready(() => {
    new EDUIO();
})

Coco.config = {
    s3: '//static.eduio.local',
    baseUrl: '/',              //server context path
    router: {
        loaderDelay: 300        // When views are swapped by Router, this time adjusts when the loading class
    },
    hbs: '../../../../hbs/',
    restService: {              //restService configuration
        path: '//api.eduio.local',             //restService path
        cacheGet: 600,          //cache time for GET Requests of same url in seconds
        cachePost: null         //cache time for GET Requests of same url in seconds
    },
    i18n:{
        locale: 'de',
        domain: ''
    },
    token: '1234'
};
