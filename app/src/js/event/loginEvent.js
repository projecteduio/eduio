var Coco = require('3m5-coco');

module.exports = dejavu.Class.declare({
    $name: 'LoginEvent',
    $extends: Coco.Event,
    _data: null,

    initialize(data, type) {
        this.$super(type);
        this._data = data;
    },
    getData(){
        return this._data;
    }

});