/**
 * (c) Johannes Klauss <johannes.klauss@3m5.de>
 * created at 14.01.14
 */

var Handlebars = require('handlebars/runtime'),
	Coco       = require('3m5-coco');


Handlebars.registerHelper("debug", function(optionalValue) {
	console.log("Current Context");
	console.log("====================");
	console.log(this);

	if (optionalValue) {
		console.log("Value");
		console.log("====================");
		console.log(optionalValue);
	}
});