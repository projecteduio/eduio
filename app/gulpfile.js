var gulp         = require('gulp'),
    plumber      = require('gulp-plumber'),
    concat       = require('gulp-concat'),
    browserify   = require('browserify'),
    autoprefixer = require('gulp-autoprefixer'),
    less         = require('gulp-less'),
    server       = require('gulp-webserver'),
    streamify    = require('gulp-streamify'),
    tap          = require('gulp-tap'),
    domain       = require('domain'),
    es6ify       = require('es6ify'),
    hbsfy        = require('hbsfy'),
    uglify       = require('gulp-uglifyjs'),
    minify       = require('gulp-minify');
    gutil        = require('gulp-util');

// Convert and concat all .less files
gulp.task('css', function() {
    gulp.src('src/less/main.less')
        .pipe(plumber())
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'IE 9'],
            cascade: false
        }))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./static/css'))
});

// Browserify, transform and concat all javascript
gulp.task('src', function() {
    gulp.src('src/js/app.js', {read:false})
        .pipe(tap(function(file) {
            var d = domain.create();

            d.on("error", function(err) {
                gutil.log(
                    gutil.colors.red("Browserify compile error:"), err.message, "\n\t", gutil.colors.cyan("in file"), file.path
                );
            });

            d.run(function() {
                file.contents = browserify({entries: [file.path]})
                    .add(es6ify.runtime)
                    .transform(es6ify.configure(/^(?!.*node_modules)+.+\.js$/))
                    .transform(hbsfy)
                    .bundle();
            });
        }))
        .pipe(streamify(concat('app.js')))
        .pipe(streamify(uglify()))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./static/js'))
});

// copy local index file to build folder
gulp.task('html', function() {
    return gulp.src(['src/html/**'])
        .pipe(gulp.dest('./build'));
});

gulp.task('rest', function() {
    return gulp.src(['src/rest/**'])
        .pipe(gulp.dest('./build/rest'));
});

// copy local index file to build folder
gulp.task('fonts', function() {
    return gulp.src(['src/fonts/**'])
        .pipe(gulp.dest('./static/fonts'));
});

// copy local index file to build folder
gulp.task('images', function() {
    return gulp.src(['src/images/**'])
        .pipe(gulp.dest('./static/images'));
});

// copy local index file to build folder
gulp.task('lang', function() {
    return gulp.src(['src/lang/**'])
        .pipe(gulp.dest('./static/lang'));
});

// clean up target folder
gulp.task('clean', function() {
    return gulp.src(["build/*", "static/*"], {read: false})
        .pipe(clean());
});

// copy local index file to build folder
gulp.task('vendor', [], function() {
    return gulp.src(['./src/js/vendor/**', './node_modules/3m5-coco/lib/vendor/dejavu/dejavu.js'])
        .pipe(gulp.dest('./static/js/vendor'));
});

gulp.task('default', function () {
    gulp.run('vendor');
    gulp.run('html');
    gulp.run('src');
    gulp.run('lang');
    gulp.run('css');
    gulp.run('fonts');
    gulp.run('images');
    gulp.run('rest');
    gulp.src(['./build', './build/js']);
    return gulp.watch(['src/**'], ['html', 'vendor', 'src', 'lang', 'images', 'fonts', 'css', 'rest']);
});
